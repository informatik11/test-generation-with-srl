public class VM {
	
	// inputs
	public boolean the_coin_sensor = false;
	public boolean _old_the_coin_sensor = false;
	public boolean the_coffee_request_sensor = false;
	public boolean _old_the_coffee_request_sensor = false;
	
	// time
	public int the_request_timer = 0;
	public int p_the_request_timer = 0;
	public int TIME = 0;
	
	// outputs
	public int the_system_mode = 0;
	public int p_the_system_mode = 0;
	public int the_coffee_machine_output = 0;
	public int p_the_coffee_machine_output = 0;
	public int _old_the_system_mode = 0;
	
	public void simulate() {
		if ((!(_old_the_system_mode == 0)) && (the_system_mode == 0)) {
			p_the_coffee_machine_output = 0;
		}
		if (((_old_the_coin_sensor == false) && (the_coin_sensor == true)) && (the_system_mode == 0)) {
			p_the_request_timer = TIME;
			p_the_system_mode = 1;
		}
		if (((_old_the_coffee_request_sensor == false) && (the_coffee_request_sensor == true)) && (the_system_mode == 1)) {
			p_the_request_timer = TIME;
			p_the_system_mode = 2;
		}
		if ((TIME-the_request_timer > 30) && (the_system_mode == 1)) {
			p_the_request_timer = TIME;
			p_the_system_mode = 3;
		}
		if (((_old_the_coffee_request_sensor == false) && (the_coffee_request_sensor == true)) && (the_system_mode == 3)) {
			p_the_request_timer = TIME;
			p_the_system_mode = 4;
		}
		if ((TIME-the_request_timer >= 10) && (the_system_mode == 2)) {
			p_the_system_mode = 5;
		}
		if (the_system_mode == 5) {
			p_the_system_mode = TIME;
			p_the_coffee_machine_output = 1;
		}
		if ((TIME-the_request_timer >= 30) && (the_system_mode == 5)) {
			p_the_system_mode = TIME;
			p_the_coffee_machine_output = 1;
		}
		if ((TIME-the_request_timer >= 30) && (the_system_mode == 4)) {
			p_the_system_mode = 6;
		}
		if (the_system_mode == 6) {
			p_the_system_mode = TIME;
			p_the_coffee_machine_output = 2;
		}
		if ((TIME-the_request_timer >= 50) && (the_system_mode == 6)) {
			p_the_system_mode = TIME;
			p_the_coffee_machine_output = 2;
		}
	}
	
}
