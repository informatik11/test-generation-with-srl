When the system mode is idle, and the system mode was not idle, the coffee machine system shall assign undefined to the coffee machine output.
When the system mode is idle, and the coin sensor is on, and the coin sensor was off, the coffee machine system shall: reset the request timer, assign choice to the system mode.
When the system mode is choice, and the coffee request sensor is on, and the coffee request sensor was off, the coffee machine system shall: reset the request timer, assign preparing weak coffee to the system mode.
When the system mode is choice, and the request timer is greater than 30, the coffee machine system shall: reset the request timer, assign late choice to the system mode.
When the system mode is late choice, and the coffee request sensor is on, and the coffee request sensor was off, the coffee machine system shall: reset the request timer, assign preparing strong coffee to the system mode.
When the system mode is preparing weak coffee, and the request timer is greater than or equal to 10, the coffee machine system shall assign dispensing weak coffee to the system mode.
When the system mode is dispensing weak coffee, the coffee machine system shall: assign idle to the system mode, assign weak to the coffee machine output.
When the system mode is dispensing weak coffee, and the request timer is greater than or equal to 30, the coffee machine system shall: assign idle to the system mode, assign weak to the coffee machine output.
When the system mode is preparing strong coffee, and the request timer is greater than or equal to 30, the coffee machine system shall assign dispensing strong coffee to the system mode.
When the system mode is dispensing strong coffee, the coffee machine system shall: assign idle to the system mode, assign strong to the coffee machine output.
When the system mode is dispensing strong coffee, and the request timer is greater than or equal to 50, the coffee machine system shall: assign idle to the system mode, assign strong to the coffee machine output.

system_mode:
idle: 0
choice: 1
late choice: 3
preparing weak coffee: 2
dispensing weak coffee: 5
preparing strong coffee: 4
dispensing strong coffee: 6

machine output:
undefined: 0
weak: 1
strong: 2