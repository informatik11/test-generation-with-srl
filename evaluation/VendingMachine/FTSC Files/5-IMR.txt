SYSTEM {
IMR {
TOPLEVEL COMPONENT IMR {
ATTRIBUTES {
ATTRIBUTE _old_the_coffee_request_sensor {
SYMBOLTYPE: GlobalVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 1
TOLERANCE: 0
}
ATTRIBUTE _old_the_coin_sensor {
SYMBOLTYPE: GlobalVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 1
TOLERANCE: 0
}
ATTRIBUTE _old_the_system_mode {
SYMBOLTYPE: GlobalVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 6
TOLERANCE: 0
}
ATTRIBUTE the_coffee_machine_output {
SYMBOLTYPE: OutputVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 2
TOLERANCE: 0
}
ATTRIBUTE the_coffee_request_sensor {
SYMBOLTYPE: InputVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 1
TOLERANCE: 0
}
ATTRIBUTE the_coin_sensor {
SYMBOLTYPE: InputVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 1
TOLERANCE: 0
}
ATTRIBUTE the_request_timer {
SYMBOLTYPE: GlobalVar
TYPE: clock
INITVALUE: 0
LOWERBOUND: 1
UPPERBOUND: 1
TOLERANCE: 0
}
ATTRIBUTE the_system_mode {
SYMBOLTYPE: OutputVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 6
TOLERANCE: 0
}
}
SC-LOCATIONS {
LOCATION NAME: IMR OID: <ARTIFICIAL>
{
}
}
}
SUT COMPONENT IMR.SUT {
SC-LOCATIONS {
LOCATION NAME: SUT OID: _SUT
{
}
}
}
SUT COMPONENT IMR.SUT.the_coffee_machine_system {
SC-LOCATIONS {
LOCATION NAME: the_coffee_machine_system OID: _the_coffee_machine_system
{
SUB-STATECHART NAME: the_coffee_machine_system OID: _the_coffee_machine_system {
LOCATION NAME: Initial OID: _Initial
{
EMANATING TRANSITIONS {
[0] --- [ true ] / ... ---> the_coffee_machine_system_L0
}
}
LOCATION NAME: the_coffee_machine_system_L0 OID: _the_coffee_machine_system_L0
{
EMANATING TRANSITIONS {
[1] --- [ (((!(IMR._old_the_system_mode == 0)) && (IMR.the_system_mode == 0)) && (IMR.the_coffee_machine_output != 0)) ] / ... ---> the_coffee_machine_system_L0
[3] --- [ ((((IMR._old_the_coin_sensor == 0) && (IMR.the_coin_sensor == 1)) && (IMR.the_system_mode == 0)) && (IMR.the_system_mode != 1)) ] / ... ---> the_coffee_machine_system_L0
[5] --- [ ((((IMR._old_the_coffee_request_sensor == 0) && (IMR.the_coffee_request_sensor == 1)) && (IMR.the_system_mode == 1)) && (IMR.the_system_mode != 2)) ] / ... ---> the_coffee_machine_system_L0
[7] --- [ ((((_timeTick - IMR.the_request_timer) > 30) && (IMR.the_system_mode == 1)) && (IMR.the_system_mode != 3)) ] / ... ---> the_coffee_machine_system_L0
[9] --- [ ((((IMR._old_the_coffee_request_sensor == 0) && (IMR.the_coffee_request_sensor == 1)) && (IMR.the_system_mode == 3)) && (IMR.the_system_mode != 4)) ] / ... ---> the_coffee_machine_system_L0
[10] --- [ ((((_timeTick - IMR.the_request_timer) >= 10) && (IMR.the_system_mode == 2)) && (IMR.the_system_mode != 5)) ] / ... ---> the_coffee_machine_system_L0
[12] --- [ ((IMR.the_system_mode == 5) && ((IMR.the_system_mode != 0) || (IMR.the_coffee_machine_output != 1))) ] / ... ---> the_coffee_machine_system_L0
[14] --- [ ((((_timeTick - IMR.the_request_timer) >= 30) && (IMR.the_system_mode == 5)) && ((IMR.the_system_mode != 0) || (IMR.the_coffee_machine_output != 1))) ] / ... ---> the_coffee_machine_system_L0
[15] --- [ ((((_timeTick - IMR.the_request_timer) >= 30) && (IMR.the_system_mode == 4)) && (IMR.the_system_mode != 6)) ] / ... ---> the_coffee_machine_system_L0
[17] --- [ ((IMR.the_system_mode == 6) && ((IMR.the_system_mode != 0) || (IMR.the_coffee_machine_output != 2))) ] / ... ---> the_coffee_machine_system_L0
[19] --- [ ((((_timeTick - IMR.the_request_timer) >= 50) && (IMR.the_system_mode == 6)) && ((IMR.the_system_mode != 0) || (IMR.the_coffee_machine_output != 2))) ] / ... ---> the_coffee_machine_system_L0
}
}
}
}
}
}
TE COMPONENT IMR.TE {
SC-LOCATIONS {
LOCATION NAME: TE OID: _TE
{
}
}
}
}
SYMBOLTABLE {
KNOWN TYPES {
Bool type 'bool' SIZE: 1
Clock type 'clock' SIZE: 0
Bool type 'bool' SIZE: 1 CONST
Floating type 'const double' SIZE: 8 CONST
Floating type 'const float' SIZE: 4 CONST
Floating type 'const long double' SIZE: 16 CONST
Integral type SIGNED 'const signed char' SIZE: 1 CONST
Integral type SIGNED 'const signed int' SIZE: 4 CONST
Integral type SIGNED 'const signed long int' SIZE: 8 CONST
Integral type SIGNED 'const signed long long int' SIZE: 8 CONST
Integral type SIGNED 'const signed short int' SIZE: 2 CONST
Integral type UNSIGNED 'const unsigned char' SIZE: 1 CONST
Integral type UNSIGNED 'const unsigned int' SIZE: 4 CONST
Integral type UNSIGNED 'const unsigned long int' SIZE: 8 CONST
Integral type UNSIGNED 'const unsigned long long int' SIZE: 8 CONST
Integral type UNSIGNED 'const unsigned short int' SIZE: 2 CONST
Floating type 'double' SIZE: 8
Floating type 'float' SIZE: 4
Floating type 'long double' SIZE: 16
Integral type SIGNED 'signed char' SIZE: 1
Integral type SIGNED 'signed int' SIZE: 4
Integral type SIGNED 'signed long int' SIZE: 8
Integral type SIGNED 'signed long long int' SIZE: 8
Integral type SIGNED 'signed short int' SIZE: 2
Integral type UNSIGNED 'unsigned char' SIZE: 1
Integral type UNSIGNED 'unsigned int' SIZE: 4
Integral type UNSIGNED 'unsigned long int' SIZE: 8
Integral type UNSIGNED 'unsigned long long int' SIZE: 8
Integral type UNSIGNED 'unsigned short int' SIZE: 2
}
VARIABLE ENTRIES {
ENTRY IMR.SUT.the_coffee_machine_system.the_coffee_machine_system.Initial {
Bool type 'bool' SIZE: 1

}
ENTRY IMR.SUT.the_coffee_machine_system.the_coffee_machine_system.the_coffee_machine_system_L0 {
Bool type 'bool' SIZE: 1

}
ENTRY IMR._old_the_coffee_request_sensor {
ATTRIBUTE _old_the_coffee_request_sensor {
SYMBOLTYPE: GlobalVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 1
TOLERANCE: 0
}
Integral type SIGNED 'signed int' SIZE: 4

}
ENTRY IMR._old_the_coin_sensor {
ATTRIBUTE _old_the_coin_sensor {
SYMBOLTYPE: GlobalVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 1
TOLERANCE: 0
}
Integral type SIGNED 'signed int' SIZE: 4

}
ENTRY IMR._old_the_system_mode {
ATTRIBUTE _old_the_system_mode {
SYMBOLTYPE: GlobalVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 6
TOLERANCE: 0
}
Integral type SIGNED 'signed int' SIZE: 4

}
ENTRY IMR.the_coffee_machine_output {
ATTRIBUTE the_coffee_machine_output {
SYMBOLTYPE: OutputVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 2
TOLERANCE: 0
}
Integral type SIGNED 'signed int' SIZE: 4

}
ENTRY IMR.the_coffee_request_sensor {
ATTRIBUTE the_coffee_request_sensor {
SYMBOLTYPE: InputVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 1
TOLERANCE: 0
}
Integral type SIGNED 'signed int' SIZE: 4

}
ENTRY IMR.the_coin_sensor {
ATTRIBUTE the_coin_sensor {
SYMBOLTYPE: InputVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 1
TOLERANCE: 0
}
Integral type SIGNED 'signed int' SIZE: 4

}
ENTRY IMR.the_request_timer {
ATTRIBUTE the_request_timer {
SYMBOLTYPE: GlobalVar
TYPE: clock
INITVALUE: 0
LOWERBOUND: 1
UPPERBOUND: 1
TOLERANCE: 0
}
Clock type 'clock' SIZE: 0

}
ENTRY IMR.the_system_mode {
ATTRIBUTE the_system_mode {
SYMBOLTYPE: OutputVar
TYPE: int
INITVALUE: 0
LOWERBOUND: 0
UPPERBOUND: 6
TOLERANCE: 0
}
Integral type SIGNED 'signed int' SIZE: 4

}
ENTRY _timeTick {
Clock type 'clock' SIZE: 0

}
}