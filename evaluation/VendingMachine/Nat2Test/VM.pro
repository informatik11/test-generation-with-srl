<?xml version="1.0" encoding="UTF-8"?>
<project>
	<name>Vending Machine</name>
	<description>Toy example.</description>
	<requirementFileName>VM.req</requirementFileName>
	<aliasFileName>VM.ali</aliasFileName>
	<dictionaryFileName>VM.dic</dictionaryFileName>

	<requirementConfiguration>
		<makeGrammarSuggestions>false</makeGrammarSuggestions>
	</requirementConfiguration>

	<cnlParserConfiguration>
		<updateLexiconData>true</updateLexiconData>
		<updateGrammarData>true</updateGrammarData>
		<verbose>true</verbose>
		<showMessages>true</showMessages>
		<showSyntaxTrees>true</showSyntaxTrees>
	</cnlParserConfiguration>

	<syntaxTreeConfiguration>
		<generateSyntaxTreeWhenSaving>true</generateSyntaxTreeWhenSaving>
		<generateSimplifiedSyntaxTree>true</generateSimplifiedSyntaxTree>
		<showOpenedSyntaxTree>true</showOpenedSyntaxTree>
	</syntaxTreeConfiguration>

	<testCaseCSPmConfiguration>
		<fdrPath>/home/ghpc/Programs/fdr2_x64</fdrPath>
		<z3Path>/home/ghpc/Programs/z3_x64</z3Path>
	</testCaseCSPmConfiguration>

	<generateAllConfiguration>
		<generateST>true</generateST>
		<generateRF>true</generateRF>
		<generateDFRS>true</generateDFRS>
		<generateSCR>false</generateSCR>
		<generateCSPm>true</generateCSPm>
		<generateTestCasesCSPm>false</generateTestCasesCSPm>
	</generateAllConfiguration>
</project>
