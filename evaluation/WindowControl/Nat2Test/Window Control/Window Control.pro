<?xml version="1.0" encoding="UTF-8"?>
<project>
	<name>Window Control</name>
	<description>Window Control</description>
	<requirementFileName>Window Control.req</requirementFileName>
	<aliasFileName>Window Control.ali</aliasFileName>
	<dictionaryFileName>Window Control.dic</dictionaryFileName>

	<requirementConfiguration>
		<makeGrammarSuggestions>true</makeGrammarSuggestions>
	</requirementConfiguration>

	<cnlParserConfiguration>
		<updateLexiconData>true</updateLexiconData>
		<updateGrammarData>true</updateGrammarData>
		<verbose>true</verbose>
		<showMessages>true</showMessages>
		<showSyntaxTrees>true</showSyntaxTrees>
	</cnlParserConfiguration>

	<syntaxTreeConfiguration>
		<generateSyntaxTreeWhenSaving>true</generateSyntaxTreeWhenSaving>
		<generateSimplifiedSyntaxTree>true</generateSimplifiedSyntaxTree>
		<showOpenedSyntaxTree>true</showOpenedSyntaxTree>
	</syntaxTreeConfiguration>

	<testCaseCSPmConfiguration>
		<fdrPath>/home/alex/Downloads/fdr</fdrPath>
		<z3Path>/home/alex/Downloads/z3-4.8.7-x64-ubuntu-16.04</z3Path>
	</testCaseCSPmConfiguration>

	<generateAllConfiguration>
		<generateST>true</generateST>
		<generateRF>true</generateRF>
		<generateDFRS>true</generateDFRS>
		<generateSCR>false</generateSCR>
		<generateCSPm>true</generateCSPm>
		<generateTestCasesCSPm>false</generateTestCasesCSPm>
	</generateAllConfiguration>
</project>
