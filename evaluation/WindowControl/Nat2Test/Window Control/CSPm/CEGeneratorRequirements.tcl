proc load { name } {
  set sess [session]
  $sess load [file dirname $name] [file tail $name]
  return $sess
}

set cspHeader [lindex $argv 0]
set cspFile [lindex $argv 1]
set numCounterExamples [lindex $argv 2]

set numReqs [lindex $argv 3]
for {set i 1} {$i <= $numReqs} {incr i} {
	set requirements($i) [lindex $argv [expr {$i + 3}]]
}

set fp [open $cspHeader r]
set cspText [read $fp]
close $fp

set fp [open $cspFile r]
set body [read $fp]
close $fp

append cspText $body
set pos [string last . $cspFile]
set cspFile [string replace $cspFile $pos $pos _FDR.]

set fp [open $cspFile "w"]
puts -nonewline $fp $cspText
close $fp

puts stdout "Loading CSP file..."
set s [load $cspFile]

puts stdout "Compiling specification..."
set S [$s compile S -t]

set init "exChoice1(S,exChoice2({"
set final "}))"

for {set i 1} {$i <= $numReqs} {incr i} {
	set reqID $requirements($i)
	puts stdout "Compiling marked specification for $reqID..."
	set z "S_"
	set z $z$reqID
	set Z [$s compile $z -t]

	puts stdout "Starting generation of counterexamples for $reqID..."
	set hyp [$Z refines $S]
	set assert [$hyp assert]
	set traces ""

	set j 1
	while {($assert == "false" || $assert == "xfalse")} {
		puts stdout "Retrieving CE$j..."

		set dbg [$hyp debug]
		set beh [$dbg attribution 0 1]
		set description [$beh describe]

		set result [string map {"\f" " " "Performs\t" "" "\v" ""} $description]	
		set result [string map {"_tau " "" " " ","} $result]
		puts stdout $result

		append traces $result
		set resp $init$traces$final
		append traces ","

		incr j
		if {$j > $numCounterExamples} {
			break
		}

		puts stdout "Recompiling specification (with counterexamples found)..."
		set T [$s compile $resp -t]

		puts stdout "Generating next counter example..."
		set hyp [$Z refines $T]
		set assert [$hyp assert]
	}
}

puts stdout "Finished!"

