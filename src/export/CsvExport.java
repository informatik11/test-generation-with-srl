package export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Input;
import model.Output;
import model.Signal;
import model.testgeneration.TestCase;
import model.testgeneration.TestSignal;
import model.testgeneration.TestSuite;

public class CsvExport {

	private String sep = ";";
	private boolean mapBooleanTo01 = true;

	public CsvExport() {

	}

	public void export(String folderPath, TestCase testCase) {
		PrintWriter pw = null;
		String shouldFail = testCase.shouldFail() ? "fail_" : "";
		try {
			File file = new File(folderPath + "/" + shouldFail + testCase.getName() + ".csv");
			pw = new PrintWriter(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		StringBuilder builder = new StringBuilder();
		builder.append("Time");
		for (TestSignal signal : testCase.getSignalList()) {
			builder.append(sep + signal.getSignal().getName());
		}
		builder.append("\n");

		// find longest signal
		int duration = 0;
		for (TestSignal signal : testCase.getSignalList()) {
			int lastTimeStamp = signal.getTimeValueMap().lastKey();
			if (lastTimeStamp > duration)
				duration = lastTimeStamp;
		}
		Map<String, String> lastValueMap = new HashMap<String, String>();
		for (int i = 0; i <= duration; i++) {
			builder.append(i);
			for (TestSignal signal : testCase.getSignalList()) {
				String curValue = signal.getTimeValueMap().get(i);
				if (curValue == null) {
					String lastValue = lastValueMap.get(signal.getSignal().getName());
					builder.append(sep + mapBoolean(lastValue));
				} else {
					lastValueMap.put(signal.getSignal().getName(), curValue);
					builder.append(sep + mapBoolean(curValue));
				}
			}
			builder.append("\n");
		}

		pw.write(builder.toString());
		pw.close();

	}

	private String mapBoolean(String curValue) {
		if (mapBooleanTo01) {
			if (curValue.equals("true"))
				return "1";
			if (curValue.equals("false"))
				return "0";
		}
		return curValue;
	}

	public void export(List<TestSuite> testSuites) {
		for (TestSuite suite : testSuites) {
			String folderPath = "tests/" + suite.getName();
			File file = new File(folderPath);
			file.mkdir();
			for (File file1 : file.listFiles()) {
				file1.delete();
			}
			for (TestCase testCase : suite.getTestCases()) {
				export(folderPath, testCase);
			}
			createMappingFile(folderPath, suite);
			System.out.println("Export finished.");
		}
	}

	private void createMappingFile(String folderPath, TestSuite suite) {
		PrintWriter pw = null;
		try {
			File file = new File(folderPath + "/mapping.csv");
			pw = new PrintWriter(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		StringBuilder builder = new StringBuilder();
		builder.append("Input;Output");
		List<String> inputs = new ArrayList<String>();
		List<String> outputs = new ArrayList<String>();
		for (TestCase test : suite.getTestCases()) {
			for (TestSignal testSignal : test.getSignalList()) {
				Signal signal = testSignal.getSignal();
				if (signal instanceof Input) {
					if (!inputs.contains(signal.getName())) {
						inputs.add(signal.getName());
					}
				} else if (signal instanceof Output) {
					if (!outputs.contains(signal.getName())) {
						outputs.add(signal.getName());
					}
				}
			}
		}
		builder.append("\n");

		int size = inputs.size();
		if (outputs.size() > size)
			size = outputs.size();
		for (int i = 0; i < size; i++) {
			if (i < inputs.size()) {
				builder.append(inputs.get(i));
			}
			if (i < outputs.size()) {
				builder.append(";" + outputs.get(i));
			}
			builder.append("\n");
		}

		pw.write(builder.toString());
		pw.close();
	}
}
