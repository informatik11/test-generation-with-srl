# put class files from SUT into MuJava/classes
javac -d ./muJava/MuJava/classes/ ./vm/VendingMachine.java
# javac -d ./muJava/MuJava/classes/ ./vm/VendingMachine_Nat2Test.java
javac -d ./muJava/MuJava/classes/ ./tis/TIS.java
javac -d ./muJava/MuJava/classes/ ./tis/TIS_Nat2Test.java

# put src files from SUT into MuJava/src
Copy-Item -Path ".\vm\VendingMachine.java" -Destination ".\muJava\MuJava\src\"
# Copy-Item -Path ".\vm\VendingMachine_Nat2Test.java" -Destination ".\muJava\MuJava\src\"
Copy-Item -Path ".\tis\TIS.java" -Destination ".\muJava\MuJava\src\"
Copy-Item -Path ".\tis\TIS_Nat2Test.java" -Destination ".\muJava\MuJava\src\"

# put src files and class files from SUT into MuJava/testset
# javac -d ./muJava/MuJava/testset/ ./vm/VendingMachine.java
# javac -d ./muJava/MuJava/testset/ ./vm/VendingMachine_Nat2Test.java
# javac -d ./muJava/MuJava/testset/ ./tis/TIS.java
# javac -d ./muJava/MuJava/testset/ ./tis/TIS_Nat2Test.java

# Copy-Item -Path ".\vm\VendingMachine.java" -Destination ".\muJava\MuJava\testset\"
# Copy-Item -Path ".\vm\VendingMachine_Nat2Test.java" -Destination ".\muJava\MuJava\testset\"
# Copy-Item -Path ".\tis\TIS.java" -Destination ".\muJava\MuJava\testset\"
# Copy-Item -Path ".\tis\TIS_Nat2Test.java" -Destination ".\muJava\MuJava\testset\"

# put class files from TestManager into MuJava/testset
cd vm/
javac -d ../muJava/MuJava/testset/ ./TestManager_VM.java
javac -d ../muJava/MuJava/testset/ ./TestManager_VM_Nat2Test.java
cd ../tis/
javac -d ../muJava/MuJava/testset/ ./TestManager_TIS.java
javac -d ../muJava/MuJava/testset/ ./TestManager_TIS_Nat2Test.java

# cd ..
# Copy-Item -Path ".\vm\TestManager_VM.java" -Destination ".\muJava\MuJava\testset\"
# Copy-Item -Path ".\vm\TestManager_VM_Nat2Test.java" -Destination ".\muJava\MuJava\testset\"
# Copy-Item -Path ".\tis\TestManager_TIS.java" -Destination ".\muJava\MuJava\testset\"
# Copy-Item -Path ".\tis\TestManager_TIS_Nat2Test.java" -Destination ".\muJava\MuJava\testset\"

pause
