package evaluation.vm;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestManager_VM {

	private List<Integer> systemMode;
	private List<Integer> machineOutput;
	private List<Integer> requestTimer;
	private List<Integer> time;
	private List<Boolean> coinSensor;
	private List<Boolean> coffeRequest;

	@Test
	public void testAllFiles() throws IOException {
		// TODO change to relative path; note: this file will be copied to a muJava
		// directory and called via muJava
		File folder = new File("D:\\Git\\gitws\\diss\\tests\\VendingMachine");
		for (File file : folder.listFiles()) {
			VendingMachine vm = new VendingMachine();
			simulate(file, vm);
		}
	}

	public void simulate(File csvFile, VendingMachine vm) throws IOException {
		if (csvFile.getName().equals("mapping.csv"))
			return;
		List<String> lines = Files.readAllLines(Paths.get(csvFile.getAbsolutePath()), StandardCharsets.UTF_8);
		int duration = lines.size() - 1;
		systemMode = new ArrayList<Integer>();
		machineOutput = new ArrayList<Integer>();
		requestTimer = new ArrayList<Integer>();
		coinSensor = new ArrayList<Boolean>();
		time = new ArrayList<Integer>();
		coffeRequest = new ArrayList<Boolean>();
		extractSignals(lines);
		boolean shouldFail = csvFile.getName().contains("fail") ? true : false;
		boolean result = true;
		for (int i = 0; i < duration; i++) {
			// for debugging purposes
			if (time.get(i) != i) {
				fail(i + ": Time was " + time.get(i));
			}
			vm.simulate(coffeRequest.get(i), coinSensor.get(i));
			// if no reset has happened,
			// increase the request timer by the time that passed during simulation, in this
			// case always 1
			if (!vm.isReset) {
				vm.the_request_timer = vm.the_request_timer + 1;
			}
			vm.old_the_coin_sensor = coinSensor.get(i);
			vm.old_the_coffee_request_button = coffeRequest.get(i);
			boolean check = checkOutputs(i, shouldFail, csvFile.getName(), vm);
			if (!check) {
				result = false;
			}
		}

		if (shouldFail && result) {
			fail(csvFile.getName() + " passed, but should have failed.");
		}
	}

	private void extractSignals(List<String> lines) {
		String header = lines.get(0);
		String[] signals = header.split(";");
		for (int i = 1; i < lines.size(); i++) {
			String currentLine = lines.get(i);
			String[] currentLineArray = currentLine.split(";");
			for (int j = 0; j < currentLineArray.length; j++) {
				if (signals[j].equals("the coin sensor")) {
					coinSensor.add(Integer.parseInt(currentLineArray[j]) == 1 ? true : false);
				}
				if (signals[j].equals("the coffee request sensor")) {
					coffeRequest.add(Integer.parseInt(currentLineArray[j]) == 1 ? true : false);
				}
				if (signals[j].equals("the request timer")) {
					requestTimer.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("the system mode")) {
					systemMode.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("the coffee machine output")) {
					machineOutput.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("Time")) {
					time.add(Integer.parseInt(currentLineArray[j]));
				}
			}
		}

	}

	private boolean checkOutputs(int i, boolean shouldFail, String string, VendingMachine vm) {
		boolean result = vm.the_system_mode == systemMode.get(i);
		if (!result && !shouldFail) {
			fail(i + ": Systemmode " + vm.the_system_mode + " but expected " + systemMode.get(i) + " in " + string);
		}
		boolean result1 = vm.the_coffee_machine_output == machineOutput.get(i);
		if (!result1 && !shouldFail) {
			fail(i + ": MachineOutput " + vm.the_coffee_machine_output + " but expected " + machineOutput.get(i)
					+ " in " + string);
		}
		boolean result2 = vm.the_request_timer == requestTimer.get(i);
		if (!result2 && !shouldFail) {
			fail(i + ": RequestTimer " + vm.the_request_timer + " but expected " + requestTimer.get(i) + " in "
					+ string);
		}
		return (result && result1 && result2);
	}

}
