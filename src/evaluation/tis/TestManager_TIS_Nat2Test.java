package evaluation.tis;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestManager_TIS_Nat2Test {

	private List<Integer> indicationLights;
	private List<Integer> flashingMode;
	private List<Float> time;
	private List<Integer> voltage;
	private List<Boolean> emergencyFlashing;
	private List<Integer> turnLever;

	@Test
	public void testAllFiles() throws IOException {
		// TODO change to relative path; note: this file will be copied to a muJava
		// directory and called via muJava
		File folder = new File("D:\\Git\\gitws\\diss\\files\\evaluation\\TurnIndicatorSystem\\TestCases");

		for (File file : folder.listFiles()) {
			TIS_Nat2Test vm = new TIS_Nat2Test();
			simulate(file, vm);
		}

	}

	public void simulate(File csvFile, TIS_Nat2Test vm) throws IOException {
		if (csvFile.getName().equals("mapping.csv"))
			return;
		List<String> lines = Files.readAllLines(Paths.get(csvFile.getAbsolutePath()), StandardCharsets.UTF_8);
		indicationLights = new ArrayList<Integer>();
		flashingMode = new ArrayList<Integer>();
		voltage = new ArrayList<Integer>();
		time = new ArrayList<Float>();
		emergencyFlashing = new ArrayList<Boolean>();
		turnLever = new ArrayList<Integer>();
		extractSignals(lines);
		boolean shouldFail = csvFile.getName().contains("fail") ? true : false;
		boolean result = true;
		double lastReset = 0;
		for (int i = 0; i < time.size(); i++) {
			vm.the_flashing_timer = time.get(i) - lastReset;
			vm.simulate(voltage.get(i), turnLever.get(i), emergencyFlashing.get(i));
			if (vm.isReset) {
				lastReset = time.get(i);
			}
			vm.old_the_emergency_flashing = emergencyFlashing.get(i);
			// because it is an output, the "old" variable refers to i-1
			if (i > 0)
				vm.old_the_flashing_mode = flashingMode.get(i - 1);
			else
				vm.old_the_flashing_mode = 2;
			vm.old_the_voltage = voltage.get(i);
			vm.old_the_turn_indicator_lever = turnLever.get(i);
			boolean check = checkOutputs(i, shouldFail, csvFile.getName(), vm);
			if (!check) {
				result = false;
			}
		}

		if (shouldFail && result) {
			fail(csvFile.getName() + " passed, but should have failed.");
		}
	}

	private void extractSignals(List<String> lines) {
		String header = lines.get(0);
		String[] signals = header.split(";");
		for (int i = 1; i < lines.size(); i++) {
			String currentLine = lines.get(i);
			if (currentLine.isEmpty())
				continue;
			String[] currentLineArray = currentLine.split(";");
			for (int j = 0; j < currentLineArray.length; j++) {
				if (signals[j].equals("the_voltage")) {
					voltage.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("the_emergency_flashing")) {
					emergencyFlashing.add(Integer.parseInt(currentLineArray[j]) == 1 ? true : false);
				}
				if (signals[j].equals("the_indication_lights")) {
					indicationLights.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("the_flashing_mode")) {
					flashingMode.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("the_turn_indicator_lever")) {
					turnLever.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("TIME")) {
					time.add(Float.parseFloat(currentLineArray[j]));
				}
			}
		}

	}

	private boolean checkOutputs(int i, boolean shouldFail, String string, TIS_Nat2Test vm) {
		boolean result = vm.the_indication_lights == indicationLights.get(i);
		if (!result && !shouldFail) {
			fail(i + ": IndicationLights " + vm.the_indication_lights + " but expected " + indicationLights.get(i)
					+ " in " + string);
		}
		boolean result1 = vm.the_flashing_mode == flashingMode.get(i);
		if (!result1 && !shouldFail) {
			fail(i + ": FlashingMode " + vm.the_flashing_mode + " but expected " + flashingMode.get(i) + " in "
					+ string);
		}
		return (result && result1);
	}

}
