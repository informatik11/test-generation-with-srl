package evaluation.tis;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestManager_TIS {

	private List<Integer> indicationLights;
	private List<Integer> flashingMode;
	private List<Integer> flashingTimer;
	private List<Integer> time;
	private List<Integer> voltage;
	private List<Boolean> emergencyFlashing;
	private List<Integer> turnLever;

	@Test
	public void testAllFiles() throws IOException {
		// TODO change to relative path; note: this file will be copied to a muJava
		// directory and called via muJava
		File folder = new File("D:\\Git\\gitws\\diss\\tests\\TIS");

		for (File file : folder.listFiles()) {
			TIS system = new TIS();
			simulate(file, system);
		}
	}

	public void simulate(File csvFile, TIS vm) throws IOException {
		if (csvFile.getName().equals("mapping.csv"))
			return;
		List<String> lines = Files.readAllLines(Paths.get(csvFile.getAbsolutePath()), StandardCharsets.UTF_8);
		int duration = lines.size() - 1;
		indicationLights = new ArrayList<Integer>();
		flashingMode = new ArrayList<Integer>();
		flashingTimer = new ArrayList<Integer>();
		voltage = new ArrayList<Integer>();
		time = new ArrayList<Integer>();
		emergencyFlashing = new ArrayList<Boolean>();
		turnLever = new ArrayList<Integer>();
		extractSignals(lines);
		boolean shouldFail = csvFile.getName().contains("fail") ? true : false;
		boolean result = true;
		for (int i = 0; i < duration; i++) {
			if (time.get(i) != i) {
				fail(i + ": Time was " + time.get(i));
			}
			vm.simulate(voltage.get(i), turnLever.get(i), emergencyFlashing.get(i));
			// if no reset has happened,
			// increase the flashing timer by the time that passed during simulation, in
			// this case always 1
			if (!vm.isReset) {
				vm.the_flashing_timer = vm.the_flashing_timer + 1;
			}
			vm.old_the_emergency_flashing = emergencyFlashing.get(i);
			// because it is an output, the "old" variable refers to i-1
			if (i > 0)
				vm.old_the_flashing_mode = flashingMode.get(i - 1);
			else
				vm.old_the_flashing_mode = 2;
			vm.old_the_voltage = voltage.get(i);
			vm.old_the_turn_indicator_lever = turnLever.get(i);
			boolean check = checkOutputs(i, shouldFail, csvFile.getName(), vm);
			if (!check) {
				result = false;

			}
		}

		if (shouldFail && result) {
			fail(csvFile.getName() + " passed, but should have failed.");
		}
	}

	private void extractSignals(List<String> lines) {
		String header = lines.get(0);
		String[] signals = header.split(";");
		for (int i = 1; i < lines.size(); i++) {
			String currentLine = lines.get(i);
			String[] currentLineArray = currentLine.split(";");
			for (int j = 0; j < currentLineArray.length; j++) {
				if (signals[j].equals("the voltage")) {
					voltage.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("emergency mode")) {
					emergencyFlashing.add(Integer.parseInt(currentLineArray[j]) == 1 ? true : false);
				}
				if (signals[j].equals("the timer")) {
					flashingTimer.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("the indication lights")) {
					indicationLights.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("the mode")) {
					flashingMode.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("the turn indicator lever")) {
					turnLever.add(Integer.parseInt(currentLineArray[j]));
				}
				if (signals[j].equals("Time")) {
					time.add(Integer.parseInt(currentLineArray[j]));
				}
			}
		}

	}

	private boolean checkOutputs(int i, boolean shouldFail, String string, TIS vm) {
		boolean result = vm.the_indication_lights == indicationLights.get(i);
		if (!result && !shouldFail) {
			fail(i + ": IndicationLights " + vm.the_indication_lights + " but expected " + indicationLights.get(i)
					+ " in " + string);
		}
		boolean result1 = vm.the_flashing_mode == flashingMode.get(i);
		if (!result1 && !shouldFail) {
			fail(i + ": FlashingMode " + vm.the_flashing_mode + " but expected " + flashingMode.get(i) + " in "
					+ string);
		}
		boolean result2 = vm.the_flashing_timer == flashingTimer.get(i);
		if (!result2 && !shouldFail) {
			fail(i + ": FlashingTimer " + vm.the_flashing_timer + " but expected " + flashingTimer.get(i) + " in "
					+ string);
		}
		return (result && result1 && result2);
	}

}
