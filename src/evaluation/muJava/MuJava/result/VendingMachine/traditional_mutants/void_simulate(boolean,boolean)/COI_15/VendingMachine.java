// This is a mutant program.
// Author : ysma

public class VendingMachine
{

    public int the_system_mode = 1;

    public int the_coffee_machine_output = 0;

    public double the_request_timer = 0;

    public boolean isReset = false;

    public boolean old_the_coin_sensor = false;

    public boolean old_the_coffee_request_button = false;

    public  void simulate( boolean the_coffee_request_button, boolean the_coin_sensor )
    {
        if (the_request_timer <= 30.0 && the_request_timer >= 10.0 && the_system_mode == 3) {
            the_coffee_machine_output = 1;
            the_system_mode = 1;
            isReset = false;
        } else {
            if (the_request_timer <= 50.0 && the_request_timer >= 30.0 && the_system_mode == 2) {
                the_coffee_machine_output = 0;
                the_system_mode = 1;
                isReset = false;
            } else {
                if (!(the_coin_sensor == true && !(old_the_coin_sensor == true)) && the_system_mode == 1) {
                    the_request_timer = 0;
                    isReset = true;
                    the_system_mode = 0;
                } else {
                    if (the_coffee_request_button == true && !(old_the_coffee_request_button == true) && old_the_coin_sensor == false && the_coin_sensor == false && the_request_timer <= 30.0 && the_system_mode == 0) {
                        the_request_timer = 0;
                        isReset = true;
                        the_system_mode = 3;
                    } else {
                        if (the_coffee_request_button == true && !(old_the_coffee_request_button == true) && old_the_coin_sensor == false && the_coin_sensor == false && the_request_timer > 30.0 && the_system_mode == 0) {
                            the_request_timer = 0;
                            isReset = true;
                            the_system_mode = 2;
                        } else {
                            isReset = false;
                        }
                    }
                }
            }
        }
    }

}
