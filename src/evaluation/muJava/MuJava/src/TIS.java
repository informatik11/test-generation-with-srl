// package evaluation.tis;

public class TIS {

	// 0: both
	// 1: left
	// 2: off
	// 3: right
	public int the_indication_lights = 2;

	// 0: both
	// 1: left
	// 2: no flashing
	// 3: right
	public int the_flashing_mode = 2;
	public double the_flashing_timer = 0;

	// turn indicator lever:
	// 0: both/idle
	// 1: left
	// 2: right
	public int old_the_turn_indicator_lever = 0;
	public boolean old_the_emergency_flashing = false;
	public int old_the_voltage = 0;

	public int old_the_flashing_mode = 2;

	public boolean isReset = false;

	public void simulate(int the_voltage, int the_turn_indicator_lever, boolean the_emergency_flashing) {
		int the_flashing_mode_temp = the_flashing_mode;
		double the_flashing_timer_temp = the_flashing_timer;
		int the_indication_lights_temp = the_indication_lights;
		isReset = false;
		if ((((!((the_flashing_mode == 3)) && (old_the_turn_indicator_lever == 2)) && (the_turn_indicator_lever == 2))
				&& (!((old_the_emergency_flashing == false)) && (the_emergency_flashing == false)))) {
			the_flashing_mode_temp = 3;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if (((the_emergency_flashing == false)
				&& (!((old_the_turn_indicator_lever == 2)) && (the_turn_indicator_lever == 2)))) {
			the_flashing_mode_temp = 3;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((!((old_the_emergency_flashing)) && (the_emergency_flashing))) {
			the_flashing_mode_temp = 0;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if (((the_emergency_flashing == false)
				&& (!((old_the_turn_indicator_lever == 1)) && (the_turn_indicator_lever == 1)))) {
			the_flashing_mode_temp = 1;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((!((old_the_turn_indicator_lever == 2)) && (the_turn_indicator_lever == 2))
				&& (old_the_emergency_flashing)) && (the_emergency_flashing))) {
			the_flashing_mode_temp = 3;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((!((the_flashing_mode == 0))
				&& (!((old_the_turn_indicator_lever == 0)) && (the_turn_indicator_lever == 0)))
				&& (old_the_emergency_flashing)) && (the_emergency_flashing))) {
			the_flashing_mode_temp = 0;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((!((old_the_turn_indicator_lever == 1)) && (the_turn_indicator_lever == 1))
				&& (old_the_emergency_flashing)) && (the_emergency_flashing))) {
			the_flashing_mode_temp = 1;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((!((the_flashing_mode == 1)) && (old_the_turn_indicator_lever == 1)) && (the_turn_indicator_lever == 1))
				&& (!((old_the_emergency_flashing == false)) && (the_emergency_flashing == false)))) {
			the_flashing_mode_temp = 1;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		// REQ001
		if ((!((old_the_voltage <= 80)) && (the_voltage <= 80))) {
			the_indication_lights_temp = 2;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		// REQ002
		if ((((the_flashing_mode == 1) && (the_voltage > 80))
				&& ((!((old_the_flashing_mode == 1)) && (the_flashing_mode == 1))
						|| (!((old_the_voltage > 80)) && (the_voltage > 80))))) {
			the_indication_lights_temp = 1;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((the_flashing_mode == 0) && (the_indication_lights == 2)) && (the_voltage > 80))
				&& (the_flashing_timer >= 22)) {
			the_indication_lights_temp = 0;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((the_flashing_mode == 1) && (the_indication_lights == 2)) && (the_voltage > 80))
				&& (the_flashing_timer >= 22)) {
			the_indication_lights_temp = 1;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((the_flashing_mode == 0) && (the_voltage > 80))
				&& ((!((old_the_flashing_mode == 0)) && (the_flashing_mode == 0))
						|| (!((old_the_voltage > 80)) && (the_voltage > 80))))) {
			the_indication_lights_temp = 0;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if (((the_flashing_mode == 2) && (the_voltage > 80))) {
			the_indication_lights_temp = 2;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((the_indication_lights == 3) || (the_indication_lights == 1)) && (the_voltage > 80))
				&& (the_flashing_timer >= 34)) {
			the_indication_lights_temp = 2;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((the_flashing_mode == 3) && (the_indication_lights == 2)) && (the_voltage > 80))
				&& (the_flashing_timer >= 22)) {
			the_indication_lights_temp = 3;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		if ((((the_flashing_mode == 3) && (the_voltage > 80))
				&& ((!((old_the_flashing_mode == 3)) && (the_flashing_mode == 3))
						|| (!((old_the_voltage > 80)) && (the_voltage > 80))))) {
			the_indication_lights_temp = 3;
			the_flashing_timer_temp = 0;
			isReset = true;
		}
		the_flashing_mode = the_flashing_mode_temp;
		the_flashing_timer = the_flashing_timer_temp;
		the_indication_lights = the_indication_lights_temp;
	}

}