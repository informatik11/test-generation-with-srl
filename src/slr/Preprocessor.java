package slr;

import java.util.List;
import java.util.Map;

import model.Input;
import model.Output;

public class Preprocessor {
	// preprocesses requirements to improve the following pipeline and information
	// extraction steps
	// e.g.:
	// 1. replace certain words with synonyms because a) SRL implementation delivers
	// better results or b) only the synonym is covered by the implemented rule-set
	// 2. extract signal data and make it available for the remaining pipeline steps
	// (from a data dictionary or similar)

	public Map<String, String> modifyRequirements(Map<String, String> idToRequirements, List<Input> inputs,
			List<Output> outputs) {
		// currently no preprocessing is implemented as necessary modifications are done
		// directly on the input requirements
		return idToRequirements;
	}

}
