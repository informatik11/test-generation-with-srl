package slr;

import java.util.List;
import java.util.Map;

import model.Enums.Strategy;
import model.Input;
import model.Output;

public class RoleLabeler {

	public SRLData analyze(Map<String, String> idToRequirements, List<Input> inputs, List<Output> outputs) {
		// use cogCompNLP
		SRLData data = new SRLData(idToRequirements, Strategy.cogCompNLP, inputs, outputs);

		return data;
	}

}
