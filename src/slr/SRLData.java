package slr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cogCompNLP.cogCompNLP;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TokenLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.View;
import model.Enums.Strategy;
import model.Function;
import model.Input;
import model.Output;
import model.PredicateFrame;
import model.RequirementFrame;
import model.Signal;
import model.testgeneration.TestSuite;

public class SRLData {

	List<RequirementFrame> requirementFrames;
	Map<String, String> idToReqMap;
	Map<String, Function> functionMap;
	List<TestSuite> testSuites;
	List<Signal> signalList;

	public SRLData(Map<String, String> idToRequirements, Strategy strategy, List<Input> inputs, List<Output> outputs) {
		signalList = new ArrayList<Signal>();
		signalList.addAll(inputs);
		signalList.addAll(outputs);
		idToReqMap = idToRequirements;
		requirementFrames = new ArrayList<RequirementFrame>();
		functionMap = new HashMap<String, Function>();
		testSuites = new ArrayList<TestSuite>();
		generateRequirementFrames(strategy);
	}

	public List<RequirementFrame> getRequirementFrames() {
		return requirementFrames;
	}

	public List<TestSuite> getTestSuites() {
		return testSuites;
	}

	public void addTestSuite(TestSuite suite) {
		this.testSuites.add(suite);
	}

	private void generateRequirementFrames(Strategy strategy) {

		if (strategy.equals(Strategy.cogCompNLP)) {
			String corpusID = "01";
			for (String id : idToReqMap.keySet()) {
				String text = idToReqMap.get(id);
				RequirementFrame reqFrame = new RequirementFrame(id, text);
				requirementFrames.add(reqFrame);
				List<View> views = cogCompNLP.generatePredicateArgumentView(corpusID, id, text);
				PredicateArgumentView slrVerb = (PredicateArgumentView) views.get(0);
				PredicateArgumentView slrComma = (PredicateArgumentView) views.get(1);
				TokenLabelView pos = (TokenLabelView) views.get(2);
				for (Constituent predicate : slrVerb.getPredicates()) {
					String verb = slrVerb.getPredicateLemma(predicate);
					// this if case handles 2 edge cases:
					// 1. some signal names may contain verbs, e.g. "move down button" - we want to
					// prevent these cases from being added as a predicate
					// 2. for some predicates, SRL might not identify any semantic roles at all - we
					// dont want to create predicate frames for these
					if (!predicateIsSignal(verb) && !predicate.getOutgoingRelations().isEmpty()) {
						PredicateFrame predicateFrame = new PredicateFrame(predicate, verb);
						for (Relation relation : predicate.getOutgoingRelations()) {
							predicateFrame.addRelation(relation.getRelationName(), relation.getTarget().toString(),
									relation.getTarget().getStartCharOffset(), relation.getTarget().getEndCharOffset());
						}
						reqFrame.addPredicateFrame(verb, predicateFrame);

						determineIfPastTense(predicateFrame, pos, predicate);
					}

				}
				// assumption: requirements consist of conditions and actions or only actions
				extractConditionActionSentenceParts(slrComma, reqFrame);
				extractAndPositions(pos, reqFrame);
			}

		}

	}

	private void extractAndPositions(TokenLabelView pos, RequirementFrame reqFrame) {
		// extract all CC tags and look whether they are "and", if yes, save their start
		// span in the requirement frame, this will later be used to identify the
		// connection between predicate frames
		for (Constituent constituent : pos.getConstituents()) {
			if (constituent.getLabel().equals("CC") && constituent.toString().toLowerCase().equals("and"))
				reqFrame.addAndStartSpan(constituent.getStartCharOffset());
			if (constituent.getLabel().equals("CC") && constituent.toString().toLowerCase().equals("or"))
				reqFrame.addOrStartSpan(constituent.getStartCharOffset());
		}
	}

	private boolean predicateIsSignal(String verb) {
		for (Signal signal : signalList) {
			if (signal.getName().equals(verb))
				return true;
		}
		return false;
	}

	private void determineIfPastTense(PredicateFrame predicateFrame, TokenLabelView pos, Constituent predicate) {
		// if the pos tagger uses "vbn" or "vbd" for a verb, the verb is in past tense,
		// for
		// present tense "vbz" is used
		// search predicate from srl in pos view with the help of span
		int startSpan = predicate.getStartSpan();
		int endSpan = predicate.getEndSpan();
		for (Constituent posConst : pos.getConstituents()) {
			if (posConst.getStartSpan() == startSpan && posConst.getEndSpan() == endSpan) {
				// extract pos information about verb
				List<String> posLabels = pos.getLabelsCovering(posConst);
				if ((posLabels.contains("VBD") || posLabels.contains("VBN"))
						&& !predicateFrame.getPredicateText().equals("set")) {
					predicateFrame.setVerbPastTense(true);
				}
				break;
			}
		}
	}

	private void extractConditionActionSentenceParts(PredicateArgumentView slrComma, RequirementFrame reqFrame) {
		// approach:
		// if requirement contains commas: use srl for commas and extract conditional
		// part via identified tags
		// check results for plausability via e.g. a keyword search
		// else apply a rule-based key word search

		// TODO implement plausability check, implement else case; both not needed for
		// the evaluated data

		// assumption: only 1 marker found by slr
		Constituent marker = null;
		for (Constituent predicate : slrComma.getPredicates()) {
			if (predicate.getAttribute("SenseNumber").equals("Introductory")) {
				marker = predicate;
				break;
			}
		}
		if (marker != null) {
			reqFrame.setConditionActionSeparator(marker);
			return;
		} else { // try substitute marker
			for (Constituent predicate : slrComma.getPredicates()) {
				if (predicate.getAttribute("SenseNumber").equals("Substitute")) {
					marker = predicate;
					break;
				}
			}
		}

		if (marker != null) {
			reqFrame.setConditionActionSeparator(marker);
			return;
		} else { // try complementary marker
			for (Constituent predicate : slrComma.getPredicates()) {
				if (predicate.getAttribute("SenseNumber").equals("Complementary")) {
					marker = predicate;
					break;
				}
			}
		}

		if (marker != null)
			reqFrame.setConditionActionSeparator(marker);

	}

	public Map<String, Function> getFunctionMap() {
		return functionMap;
	}

}
