package z3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.microsoft.z3.ApplyResult;
import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.Goal;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.Model;
import com.microsoft.z3.Optimize;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Sort;
import com.microsoft.z3.Status;
import com.microsoft.z3.Tactic;

import model.Enums.ComparisonOperator;
import model.Enums.DataType;
import model.Enums.MathOperator;
import model.Function;
import model.Input;
import model.Output;
import model.Signal;
import model.expressions.AndExpression;
import model.expressions.AtomicExpression;
import model.expressions.BoolExpression;
import model.expressions.ImplicationExpression;
import model.expressions.NotExpression;
import model.expressions.OrExpression;
import model.expressions.Z3Signal;
import model.testgeneration.TestCase;
import model.testgeneration.TestSignal;
import model.testgeneration.TestSuite;
import slr.SRLData;

public class Generator {

	private TreeMap<String, Z3Signal> signals;
	private int counter;
	private TreeMap<Integer, BoolExpr> implicationAntecedentMap;
	private TreeMap<Integer, BoolExpr> implicationConsequentMap;
	private int implicationID = 0;
	private Map<Z3Signal, List<Integer>> consequentSignalList;
	private Map<Z3Signal, Map<Integer, List<BoolExpr>>> outputSignalToTimeStepToAntecedentMap;
	private int maxSignalSize;
	private int testCaseID = 1;
	private boolean isConsequent;
	private boolean isAntecedent;
	private int idOfNegatedExpression;
	private BoolExpression negatedExpr;
	// a worst case estimation for the number of iterations necessary to reach the
	// desired output state for a test case
	private int estimatedIterationsToOutputState;
	private IntExpr outputStateTimePoint;
	private IntExpr outputStateTimePoint_t2;
	private OperatorOrGeneration orVariationGen = OperatorOrGeneration.normal;
	private OrGeneration orGen = OrGeneration.extended;
	private int lowerBound;
	private int upperBound;
	private TreeMap<Integer, BoolExpr> antecedentToAlternateVarAntecedentMap;
	private boolean generateAndCombinations = false;

	private enum OperatorOrGeneration {
		none, normal, not;
	}

	private enum OrGeneration {
		none, extended;
	}

	@SuppressWarnings("serial")
	class TestFailedException extends Exception {
		public TestFailedException() {
			super("Check FAILED");
		}
	};

	private Model checkAndMinimize(Optimize opt, IntExpr variableToMinimize) {

		Optimize.Handle mt1 = opt.MkMinimize(variableToMinimize);
		Status status = opt.Check();
		if (status == Status.SATISFIABLE) {
			return opt.getModel();
		} else if (status == Status.UNKNOWN) {
			System.err.println("Model unknown.");
			return null;
		} else if (status == Status.UNSATISFIABLE) {
			System.out.println("Model unsat.");
			return null;
		}
		return null;
	}

	private Model check(Solver s) {
		Status status = s.check();
		if (status == Status.SATISFIABLE) {
			return s.getModel();
		} else if (status == Status.UNKNOWN) {
			System.err.println("Model unknown.");
			return null;
		} else if (status == Status.UNSATISFIABLE) {
			System.out.println("Model unsat.");
			return null;
		}
		return null;
	}

	public void generateTestSuite(SRLData data, List<Input> inputs, List<Output> outputs,
			int estimationToReachOutputState) {
		long startTime = System.nanoTime();
		this.estimatedIterationsToOutputState = estimationToReachOutputState;
		for (String functionName : data.getFunctionMap().keySet()) {
			Function function = data.getFunctionMap().get(functionName);

			BoolExpression originalExp = function.generateExpression();
			LinkedHashMap<BoolExpression, BoolExpression> expressionsWithAntecedentNotConsequent = function
					.generateFailExpressions();

			TestSuite suite = generateTestSuite_internal(originalExp, expressionsWithAntecedentNotConsequent, function);

			data.addTestSuite(suite);
		}
		long endTime = System.nanoTime();
		long elapsedTime = endTime - startTime;
		System.out.println("Execution time (sec): " + elapsedTime / 1000000000);

	}

	private TestSuite generateTestSuite_internal(BoolExpression originalExp,
			LinkedHashMap<BoolExpression, BoolExpression> expressionsWithAntecedentNotConsequent, Function function) {
		TestSuite testSuite = new TestSuite(function.getSystemName());
		// add test cases for antecedent => consequent, these need to pass
		// testSuite.addTestCases(generateTestCases(originalExp, function, null,
		// false));
		// generate test cases where the initial val changes at t2 < t
		orVariationGen = OperatorOrGeneration.normal;
		orGen = OrGeneration.extended;
		generateAndCombinations = true;
		testSuite.addTestCases(generateTestCases(originalExp, function, null, true));
		// create chained test cases
		orVariationGen = OperatorOrGeneration.none;
		orGen = OrGeneration.none;
		int estimatedIterationsToOutputState_Original = estimatedIterationsToOutputState;
		estimatedIterationsToOutputState = estimatedIterationsToOutputState * 2;
		// testSuite.addTestCases(generateChainedTestCases(originalExp, function));
		// add test combinations for antecedent => not ( consequent), these need to fail
		estimatedIterationsToOutputState = estimatedIterationsToOutputState_Original;
		orVariationGen = OperatorOrGeneration.normal;
		orGen = OrGeneration.extended;
		generateAndCombinations = false;
		for (BoolExpression negatedExp : expressionsWithAntecedentNotConsequent.keySet()) {
			BoolExpression exp = expressionsWithAntecedentNotConsequent.get(negatedExp);
			testSuite.addTestCases(generateTestCases(exp, function, negatedExp, true));
		}
		return testSuite;
	}

	private List<TestCase> generateChainedTestCases(BoolExpression exp, Function function) {
		List<TestCase> testCaseList = new ArrayList<TestCase>();
		HashMap<String, String> cfg = new HashMap<String, String>();
		cfg.put("model", "true");
		Context ctx = new Context(cfg);
		BoolExpr systemConstraints = generateSystemConstraints(exp, null, function, false, ctx);
		for (Integer id : implicationAntecedentMap.keySet()) {
			BoolExpr antecedent = implicationAntecedentMap.get(id);
			for (Integer id2 : implicationAntecedentMap.keySet()) {
				BoolExpr varConstraints = createVariableConstraints(ctx, id2);
				List<BoolExpr> orVariations = generateOrVariations(ctx, antecedent);
				for (BoolExpr orVariation : orVariations) {
					Optimize solver = ctx.mkOptimize();
					solver.Add(ctx.mkAnd(orVariation, varConstraints, systemConstraints));
					generateTests(ctx, testCaseList, antecedent, solver, null);
				}
			}
		}
		return testCaseList;
	}

	private BoolExpr generateSystemConstraints(BoolExpression exp, BoolExpression negatedExp, Function function,
			boolean initialValChange, Context ctx) {
		initGlobalVars(function, ctx);
		negatedExpr = negatedExp;
		BoolExprTuple boolExp = createZ3Expressions(exp, ctx, function, null);
		BoolExpr exprWithInstantiatedTime = boolExp.timeExpr;
		BoolExpr minMaxConstraints = createMinMaxAssertions(ctx);
		BoolExpr initialValConstraints = createInitialValAssertions(ctx, initialValChange);
		BoolExpr outputStateTimePointConstraints = createOutputStateTimePointConstraints(ctx, outputStateTimePoint);
		BoolExpr ifThenElseConstraints = createIfThenElseConstraints(ctx);
		BoolExpr result = ctx.mkAnd(ifThenElseConstraints, outputStateTimePointConstraints, minMaxConstraints,
				initialValConstraints, exprWithInstantiatedTime);
		return result;
	}

	private List<TestCase> generateTestCases(BoolExpression exp, Function function, BoolExpression negatedExp,
			boolean initialValChange) {
		List<TestCase> testCaseList = new ArrayList<TestCase>();
		HashMap<String, String> cfg = new HashMap<String, String>();
		cfg.put("model", "true");
		Context ctx = new Context(cfg);
		BoolExpr systemConstraints = generateSystemConstraints(exp, negatedExp, function, initialValChange, ctx);
		// produce multiple goals by adding one "and" with one antecedent
		// set to true
		// (at time point 0), the split-clause strategy will then split these goals into
		// multiple subgoals for each "or" contained in the antecedent
		// this avoids "useless" test cases that set all antecedents to false
		// if test cases that should fail are generated, the only generate one test case
		// with the clause that was negated
		// added so that the number of total test cases is limited
		for (Integer id : implicationAntecedentMap.keySet()) {
			if (negatedExp == null || id == idOfNegatedExpression) {
				BoolExpr antecedent = implicationAntecedentMap.get(id);

				List<BoolExpr> andCombinations = createAndCombinations(antecedent, ctx);
				for (BoolExpr andCombination : andCombinations) {
					Optimize solver = ctx.mkOptimize();
					solver.Add(ctx.mkAnd(andCombination, systemConstraints));
					generateTests(ctx, testCaseList, antecedent, solver, negatedExp);
				}
				List<BoolExpr> orVariations = generateOrVariations(ctx, antecedent);
				for (BoolExpr orVariation : orVariations) {
					Optimize solver = ctx.mkOptimize();
					solver.Add(ctx.mkAnd(orVariation, systemConstraints));
					generateTests(ctx, testCaseList, antecedent, solver, negatedExp);
				}
			}
		}
		return testCaseList;
	}

	private void generateTests(Context ctx, List<TestCase> testCaseList, BoolExpr antecedent, Optimize solver,
			BoolExpression negatedExp) {
		Model model = checkAndMinimize(solver, outputStateTimePoint);
		if (negatedExp != null)
			System.out.println("Expected to fail:");
		System.out.println("Antecedent: " + antecedent);
		if (model != null) {
			Expr outputStateAchieved = model.evaluate(outputStateTimePoint, false);
			int outputStateAchievedTime = Double.valueOf(outputStateAchieved.toString()).intValue();
			System.out.println("t = " + outputStateAchievedTime);
			TestCase testCase = new TestCase("TestCase" + testCaseID);
			testCase.setShouldFail(negatedExp == null ? false : true);
			testCaseList.add(testCase);
			StringBuilder builder = new StringBuilder();
			builder.append(negatedExp != null ? "Fail " + testCaseID + "\n" : testCaseID + "\n");
			// builder.append(antecedentAndConsequent.get(0) + " => " +
			// antecedentAndConsequent.get(1) + "\n");
			for (String var : signals.keySet()) {
				Z3Signal signal = signals.get(var);
				TestSignal testSignal = new TestSignal(signal.getSignal());
				FuncDecl valueFunc = signal.getValueFunc();
				for (int i1 = 0; i1 <= outputStateAchievedTime + maxSignalSize; i1++) {
					// (f i), respectively (eval (f i)) will be done via model.evaluate(..)
					Expr evalValueExpr = ctx.mkApp(valueFunc, ctx.mkInt(i1));
					Expr value = model.evaluate(evalValueExpr, false);
					testSignal.addTimeValuePair(i1, value.toString());
					builder.append(var + "(" + i1 + ")" + "=" + value);
					builder.append("\n");
				}
				testCase.addSignal(testSignal);
			}
			// System.out.println(builder.toString());
			testCaseID = testCaseID + 1;
		}
	}

	private List<BoolExpr> createAndCombinations(BoolExpr antecedent, Context ctx) {
		List<BoolExpr> result = new ArrayList<BoolExpr>();
		if (antecedent.isAnd() && generateAndCombinations) {
			Expr[] args = antecedent.getArgs();
			for (int i = 0; i < args.length; i++) {
				BoolExpr not = ctx.mkNot((BoolExpr) args[i]);
				BoolExpr[] andElements = new BoolExpr[(args.length - 1)];
				int index = 0;
				for (int j = 0; j < args.length; j++) {
					if (i != j) {
						andElements[index] = (BoolExpr) args[j];
						index++;
					}
				}
				BoolExpr and = ctx.mkAnd(andElements);
				BoolExpr result1 = ctx.mkAnd(and, not);
				result.add(result1);
			}
		}
		return result;
	}

	private BoolExpr createVariableConstraints(Context ctx, Integer id) {
		BoolExpr alternateAntecedent = antecedentToAlternateVarAntecedentMap.get(id);
		// >=0
		BoolExpr greater = ctx.mkGe(outputStateTimePoint_t2, ctx.mkInt(0));
		// t2 < t
		BoolExpr less = ctx.mkLt(outputStateTimePoint_t2, outputStateTimePoint);
		return ctx.mkAnd(alternateAntecedent, less, greater);
	}

	private void initGlobalVars(Function function, Context ctx) {
		maxSignalSize = function.getLongestSignalLength() - 1;
		lowerBound = -2;
		upperBound = estimatedIterationsToOutputState + maxSignalSize;
		signals = new TreeMap<String, Z3Signal>();
		implicationAntecedentMap = new TreeMap<Integer, BoolExpr>();
		implicationConsequentMap = new TreeMap<Integer, BoolExpr>();
		antecedentToAlternateVarAntecedentMap = new TreeMap<Integer, BoolExpr>();
		outputSignalToTimeStepToAntecedentMap = new HashMap<Z3Signal, Map<Integer, List<BoolExpr>>>();
		implicationID = 0;
		counter = 3;
		idOfNegatedExpression = -1;
		outputStateTimePoint = ctx.mkIntConst("t");
		outputStateTimePoint_t2 = ctx.mkIntConst("t2");
	}

	// if no condition holds true that would change an output, assume that the
	// output
	// does not change / keeps it value.
	// Exception: timers are incremented by 1 if no condition holds that directly
	// influences the timer
	private BoolExpr createIfThenElseConstraints(Context ctx) {
		List<Expr> ifThenElses = new ArrayList<Expr>();
		BoolExpr trueExpr = ctx.mkTrue();
		for (Z3Signal signal : outputSignalToTimeStepToAntecedentMap.keySet()) {
			Map<Integer, List<BoolExpr>> timeStepToAntecedentMap = outputSignalToTimeStepToAntecedentMap.get(signal);
			for (int i = lowerBound; i <= upperBound; i++) {
				BoolExpr holdValue = null;
				if (signal.getSignal().getDatatype().equals(DataType.timer)) {
					ArithExpr incrementByOne = ctx.mkAdd((ArithExpr) ctx.mkApp(signal.getValueFunc(), ctx.mkInt(i - 1)),
							ctx.mkInt(1));
					holdValue = ctx.mkEq((ctx.mkApp(signal.getValueFunc(), ctx.mkInt(i))), incrementByOne);
				} else {
					holdValue = ctx.mkEq((ctx.mkApp(signal.getValueFunc(), ctx.mkInt(i))),
							ctx.mkApp(signal.getValueFunc(), ctx.mkInt(i - 1)));
				}

				if (timeStepToAntecedentMap.containsKey(i)) {
					BoolExpr orOfConditions = ctx
							.mkOr(outputSignalToTimeStepToAntecedentMap.get(signal).get(i).toArray(new BoolExpr[0]));
					ifThenElses.add(ctx.mkITE(orOfConditions, trueExpr, holdValue));
				} else {
					ifThenElses.add(holdValue);
				}
			}
		}
		return ctx.mkAnd(ifThenElses.toArray(new BoolExpr[0]));
	}

	private BoolExpr createOutputStateTimePointConstraints(Context ctx, IntExpr outputStateTimePoint) {
		// >=0
		BoolExpr greater = ctx.mkGe(outputStateTimePoint, ctx.mkInt(0));
		// <= worst case
		BoolExpr less = ctx.mkLe(outputStateTimePoint, ctx.mkInt(upperBound));
		return ctx.mkAnd(less, greater);
	}

	private List<BoolExpr> generateOrVariations(Context ctx, BoolExpr boolExpr) {
		// create or variations via split-clause tactic
		Goal g = ctx.mkGoal(true, false, false);
		g.add(boolExpr);

		Tactic t1 = ctx.repeat(ctx.orElse(ctx.mkTactic("split-clause"), ctx.skip()), counter);
		ApplyResult ar = t1.apply(g);
		List<BoolExpr> orVariations = new ArrayList<BoolExpr>();
		Goal[] subgoals = ar.getSubgoals();

		for (int i = 0; i < subgoals.length; i++) {
			BoolExpr formulas = ctx.mkAnd(subgoals[i].getFormulas());
			// add normal subgoal containing only i true
			orVariations.add(formulas);
		}
		return orVariations;
	}

	private boolean contains(BoolExpr[] allFormulas, BoolExpr formula) {
		boolean isContained = false;
		for (BoolExpr expr : allFormulas) {
			if (expr.toString().equals(formula.toString())) {
				isContained = true;
				break;
			}
		}
		return isContained;
	}

	private boolean containsOnlyOutputs(List<Z3Signal> list) {
		for (Z3Signal z3Signal : list) {
			if (z3Signal.getSignal() instanceof Input) {
				return false;
			}
		}
		return true;
	}

	private boolean containsOnlyInputs(List<Z3Signal> list) {
		for (Z3Signal z3Signal : list) {
			if (z3Signal.getSignal() instanceof Output) {
				return false;
			}
		}
		return true;
	}

	private BoolExpr createInitialValAssertions(Context ctx, boolean initialValChange) {
		List<BoolExpr> initialValExpressions = new ArrayList<BoolExpr>();
		for (String signalName : signals.keySet()) {
			Z3Signal signal = signals.get(signalName);
			FuncDecl valueFunc = signal.getValueFunc();
			String initialValue = signal.getSignal().getInitialValue();
			BoolExpr initialValTMinus1 = createInitialValExpr(ctx, signal, valueFunc, initialValue, -1);
			initialValExpressions.add(initialValTMinus1);
			if (signal.getSignal() instanceof Input) {
				BoolExpr initialValT0 = createInitialValExpr(ctx, signal, valueFunc, initialValue, 0);
				initialValExpressions.add(initialValT0);
			} else {
				if (signal.getSignal().getDatatype().equals(DataType.timer)) {
					Integer value = Double.valueOf(initialValue).intValue();
					value -= 1;
					BoolExpr initialValTMinus2 = createInitialValExpr(ctx, signal, valueFunc, value.toString(), -2);
					initialValExpressions.add(initialValTMinus2);
				} else {
					BoolExpr initialValTMinus2 = createInitialValExpr(ctx, signal, valueFunc, initialValue, -2);
					initialValExpressions.add(initialValTMinus2);
				}

			}

			if (initialValChange) {
				// add x != initial val at some timepoint t2 with t2 < t
				IntExpr t2 = ctx.mkIntConst("t_" + signalName);
				BoolExpr initialValChangeExpr;
				if (signal.getSignal().getDatatype().equals(DataType.bool)) {
					initialValChangeExpr = ctx
							.mkNot(ctx.mkEq(ctx.mkApp(valueFunc, t2), ctx.mkBool(Boolean.parseBoolean(initialValue))));
				} else {
					initialValChangeExpr = ctx.mkNot(
							ctx.mkEq(ctx.mkApp(valueFunc, t2), ctx.mkInt(Double.valueOf(initialValue).intValue())));
				}
				BoolExpr t2Greater0 = ctx.mkGt(t2, ctx.mkInt(0));
				BoolExpr t2LowerT = ctx.mkLt(t2, outputStateTimePoint);
				initialValExpressions.add(t2LowerT);
				initialValExpressions.add(t2Greater0);
				initialValExpressions.add(initialValChangeExpr);
			}

		}
		BoolExpr result = ctx.mkAnd(initialValExpressions.toArray(new BoolExpr[0]));
		return result;
	}

	private BoolExpr createInitialValExpr(Context ctx, Z3Signal signal, FuncDecl valueFunc, String initialValue,
			int time) {
		IntExpr timeForInitialVal = ctx.mkInt(time);
		BoolExpr initialVal;
		if (signal.getSignal().getDatatype().equals(DataType.bool)) {
			initialVal = ctx.mkEq(ctx.mkApp(valueFunc, timeForInitialVal),
					ctx.mkBool(Boolean.parseBoolean(initialValue)));
		} else {
			initialVal = ctx.mkEq(ctx.mkApp(valueFunc, timeForInitialVal),
					ctx.mkInt(Double.valueOf(initialValue).intValue()));
		}
		return initialVal;
	}

	private BoolExpr createMinMaxAssertions(Context ctx) {
		List<BoolExpr> minMaxExpressions = new ArrayList<BoolExpr>();
		for (String signalName : signals.keySet()) {
			Z3Signal signal = signals.get(signalName);
			FuncDecl valueFunc = signal.getValueFunc();
			if (!signal.getSignal().getDatatype().equals(DataType.bool)) {
				String minValue = signal.getSignal().getLowerLimit();
				String maxValue = signal.getSignal().getUpperLimit();
				if (!minValue.isEmpty() && !maxValue.isEmpty()) {
					// (assert (forall ((t Int))
					// (=> (and (<= t 2) (>= t -1))
					// (and (<= (f t) maxValue) (>= (f t) minValue))
					// ))
					// avoid for all by adding an assertion for each timepoint between 0 and worst
					// case estimation + maxSignalSize
					for (int i = lowerBound; i <= upperBound; i++) {
						IntExpr iterator = ctx.mkInt(i);
						BoolExpr valueGreaterThanMin = ctx.mkGe((ArithExpr) ctx.mkApp(valueFunc, iterator),
								ctx.mkInt(Integer.valueOf(minValue)));
						BoolExpr valueLessThanMax = ctx.mkLe((ArithExpr) ctx.mkApp(valueFunc, iterator),
								ctx.mkInt(Integer.valueOf(maxValue)));
						minMaxExpressions.add(valueLessThanMax);
						minMaxExpressions.add(valueGreaterThanMin);

					}
				}
			}
		}
		BoolExpr result = ctx.mkAnd(minMaxExpressions.toArray(new BoolExpr[0]));
		return result;
	}

	/**
	 * generates the z3 expression containing "or" combinations for different test
	 * cases, e.g. instead of x >= 80 it will contain (x>80 or x= 80 or x=maxValue)
	 * 
	 * @param exp
	 * @param ctx
	 * @param function
	 * @return
	 */
	private BoolExprTuple createZ3Expressions(BoolExpression exp, Context ctx, Function function, IntExpr timePoint) {
		if (exp instanceof AndExpression) {
			List<BoolExpr> boolExpr = new ArrayList<BoolExpr>();
			List<BoolExpr> boolExprWithT = new ArrayList<BoolExpr>();
			List<BoolExpr> boolExprWithT2 = new ArrayList<BoolExpr>();
			for (BoolExpression exp2 : ((AndExpression) exp).getExpressions()) {
				BoolExprTuple tuple = createZ3Expressions(exp2, ctx, function, timePoint);
				boolExpr.add(tuple.timeExpr);
				boolExprWithT.add(tuple.generalExpr);
				boolExprWithT2.add(tuple.generalExpr_t2);
				counter += 1;
			}
			BoolExpr result = null;
			BoolExpr resultWithT = null;
			BoolExpr resultWithT2 = null;
			if (boolExpr.size() > 1) {
				result = ctx.mkAnd(boolExpr.toArray(new BoolExpr[0]));
				resultWithT = ctx.mkAnd(boolExprWithT.toArray(new BoolExpr[0]));
				resultWithT2 = ctx.mkAnd(boolExprWithT2.toArray(new BoolExpr[0]));
			} else {
				if (boolExpr.isEmpty())
					return new BoolExprTuple(ctx.mkTrue(), ctx.mkTrue(), ctx.mkTrue());
				else {
					result = boolExpr.get(0);
					resultWithT = boolExprWithT.get(0);
					resultWithT2 = boolExprWithT2.get(0);
				}
			}

			return new BoolExprTuple(resultWithT, result, resultWithT2);
		}
		if (exp instanceof OrExpression) {
			List<BoolExpr> boolExpr = new ArrayList<BoolExpr>();
			List<BoolExpr> boolExprWithT = new ArrayList<BoolExpr>();
			List<BoolExpr> boolExprWithT2 = new ArrayList<BoolExpr>();
			for (BoolExpression exp2 : ((OrExpression) exp).getExpressions()) {
				BoolExprTuple tuple = createZ3Expressions(exp2, ctx, function, timePoint);
				boolExpr.add(tuple.timeExpr);
				boolExprWithT.add(tuple.generalExpr);
				boolExprWithT2.add(tuple.generalExpr_t2);
				counter += 1;
			}
			BoolExpr result = null;
			BoolExpr resultWithT = null;
			BoolExpr resultWithT2 = null;
			if (boolExpr.size() > 1) {
				result = ctx.mkOr(boolExpr.toArray(new BoolExpr[0]));
				if (orGen == OrGeneration.none) {
					resultWithT = ctx.mkOr(boolExprWithT.toArray(new BoolExpr[0]));
				} else if (orGen == OrGeneration.extended) {
					resultWithT = generateOrVariations(ctx, boolExprWithT);
				}
				resultWithT2 = ctx.mkOr(boolExprWithT2.toArray(new BoolExpr[0]));
			} else {
				result = boolExpr.get(0);
				resultWithT = boolExprWithT.get(0);
				resultWithT2 = boolExprWithT2.get(0);
			}
			return new BoolExprTuple(resultWithT, result, resultWithT2);
		}
		if (exp instanceof NotExpression) {
			BoolExpression exp2 = ((NotExpression) exp).getExpression();
			BoolExprTuple tuple = createZ3Expressions(exp2, ctx, function, timePoint);
			BoolExpr result = ctx.mkNot(tuple.timeExpr);
			BoolExpr resultWithT = ctx.mkNot(tuple.generalExpr);
			BoolExpr resultWithT2 = ctx.mkNot(tuple.generalExpr_t2);
			counter += 1;
			return new BoolExprTuple(resultWithT, result, resultWithT2);

		}
		if (exp instanceof ImplicationExpression) {

			implicationID += 1;

			if (exp == negatedExpr) {
				idOfNegatedExpression = implicationID;
			}

			// (forall ((t Int) (t2 Int))
			// (=> (and (<= t 2) (>= t -1))
			// (=> antecedent consequent timeexpression) ))))
			// avoid for all by adding an assertion for each timepoint between 0 and worst
			// case estimation + maxSignalSize
			BoolExpression antecedentExp = ((ImplicationExpression) exp).getAntecedent();
			BoolExpression consequentExp = ((ImplicationExpression) exp).getConsequent();
			List<BoolExpr> implicationList = new ArrayList<BoolExpr>();
			BoolExpr implicationWithT = null;
			BoolExpr implicationWithT2 = null;
			for (int i = lowerBound; i <= upperBound; i++) {
				IntExpr iterator = ctx.mkInt(i);
				isAntecedent = true;
				BoolExprTuple antecedent = createZ3Expressions(antecedentExp, ctx, function, iterator);
				isAntecedent = false;
				isConsequent = true;
				consequentSignalList = new HashMap<Z3Signal, List<Integer>>();
				BoolExprTuple consequent = createZ3Expressions(consequentExp, ctx, function, iterator);
				isConsequent = false;
				// for every output signal contained in the consequent, save the antecedent
				for (Z3Signal signal : consequentSignalList.keySet()) {
					for (Integer referencedTimePoint : consequentSignalList.get(signal)) {
						if (!outputSignalToTimeStepToAntecedentMap.containsKey(signal))
							outputSignalToTimeStepToAntecedentMap.put(signal, new HashMap<Integer, List<BoolExpr>>());
						if (!outputSignalToTimeStepToAntecedentMap.get(signal).containsKey(referencedTimePoint))
							outputSignalToTimeStepToAntecedentMap.get(signal).put(referencedTimePoint,
									new ArrayList<BoolExpr>());
						outputSignalToTimeStepToAntecedentMap.get(signal).get(referencedTimePoint)
								.add(antecedent.timeExpr);
					}
				}
				if (i == 0) {
					BoolExpr antecedentWithT = antecedent.generalExpr;
					BoolExpr consequentWithT = consequent.generalExpr;
					implicationAntecedentMap.put(implicationID, antecedentWithT);
					implicationConsequentMap.put(implicationID, consequentWithT);
					antecedentToAlternateVarAntecedentMap.put(implicationID, antecedent.generalExpr_t2);
					implicationWithT = ctx.mkImplies(antecedentWithT, consequentWithT);
					implicationWithT2 = ctx.mkImplies(antecedent.generalExpr_t2, consequent.generalExpr_t2);
				}
				BoolExpr implication = ctx.mkImplies(antecedent.timeExpr, consequent.timeExpr);
				implicationList.add(implication);
			}
			BoolExpr result = ctx.mkAnd(implicationList.toArray(new BoolExpr[0]));
			counter += 2;

			return new BoolExprTuple(implicationWithT, result, implicationWithT2);

		}
		if (exp instanceof AtomicExpression) {
			Signal signal = ((AtomicExpression) exp).getSignal();

			Z3Signal z3Signal = null;
			if (signals.containsKey(signal.getName())) {
				z3Signal = signals.get(signal.getName());
			} else {
				Sort dataType;
				if (signal.getDatatype().equals(DataType.bool)) {
					dataType = ctx.getBoolSort();
				} else {
					dataType = ctx.getIntSort();
				}
				z3Signal = new Z3Signal(signal, ctx.mkFuncDecl(signal.getName(), ctx.getIntSort(), dataType));
				signals.put(signal.getName(), z3Signal);
			}

			ComparisonOperator operator = ((AtomicExpression) exp).getOperator();
			String value = ((AtomicExpression) exp).getValue();
			int timeOffset = ((AtomicExpression) exp).getTimeOffset();
			int valueTimeOffset = ((AtomicExpression) exp).getValueTimeOffset();
			MathOperator mathOperator = ((AtomicExpression) exp).getMathOperator();
			int afterValue = ((AtomicExpression) exp).getAfter();

			FuncDecl valueFunc = z3Signal.getValueFunc();
			BoolExpr result = null;
			int currentTimePoint = Double.valueOf(timePoint.toString()).intValue();
			if (afterValue != 0) {
				List<BoolExpr> afterVals = new ArrayList<BoolExpr>();
				int newTimePoint = currentTimePoint + afterValue;
				afterVals.add(createAtomicExpr(ctx, signal, operator, value, timeOffset, valueTimeOffset, mathOperator,
						valueFunc, ctx.mkInt(newTimePoint)));
				result = ctx.mkAnd(afterVals.toArray(new BoolExpr[0]));
			} else {
				result = createAtomicExpr(ctx, signal, operator, value, timeOffset, valueTimeOffset, mathOperator,
						valueFunc, timePoint);
			}
			BoolExpr exprWithT = createAtomicExpr(ctx, signal, operator, value, timeOffset, valueTimeOffset,
					mathOperator, valueFunc, outputStateTimePoint);
			BoolExpr exprWithT2 = createAtomicExpr(ctx, signal, operator, value, timeOffset, valueTimeOffset,
					mathOperator, valueFunc, outputStateTimePoint_t2);
			if (isConsequent && signal instanceof Output) {
				if (!consequentSignalList.containsKey(z3Signal))
					consequentSignalList.put(z3Signal, new ArrayList<Integer>());
				consequentSignalList.get(z3Signal).add(currentTimePoint + timeOffset + afterValue);
			}

			counter += 3;
			return new BoolExprTuple(exprWithT, result, exprWithT2);
		}
		return null;
	}

	private BoolExpr generateOrVariations(Context ctx, List<BoolExpr> exprList) {
		List<BoolExpr> result = new ArrayList<BoolExpr>();
		for (int i = 0; i < exprList.size(); i++) {
			// add i true and others dont care
			result.add(exprList.get(i));
			// add i true and others false
			List<BoolExpr> iTrue = new ArrayList<BoolExpr>();
			iTrue.add(exprList.get(i));
			for (int j = 0; j < exprList.size(); j++) {
				if (i != j) {
					iTrue.add(ctx.mkNot(exprList.get(j)));
				}
			}
			result.add(ctx.mkAnd(iTrue.toArray(new BoolExpr[0])));
		}
		// add all true
		BoolExpr allTrue = ctx.mkAnd(exprList.toArray(new BoolExpr[0]));
		result.add(allTrue);
		return ctx.mkOr(result.toArray(new BoolExpr[0]));
	}

	private BoolExpr createAtomicExpr(Context ctx, Signal signal, ComparisonOperator operator, String value,
			int timeOffset, int valueTimeOffset, MathOperator mathOperator, FuncDecl valueFunc, IntExpr timePoint) {
		// determine index/offset, e.g. f (- t 1)
		Expr functionIndex = null;
		// functionIndexValue is used if the verb is "changes" or if there is a
		// math operator
		Expr functionIndexValue = null;
		if (timeOffset == 0) {
			functionIndex = timePoint;
		} else if (timeOffset < 0) {
			functionIndex = ctx.mkSub(timePoint, ctx.mkInt(timeOffset * -1));
		} else if (timeOffset > 0) {
			functionIndex = ctx.mkAdd(timePoint, ctx.mkInt(timeOffset));
		}
		// the value time offset is relative to the time offset
		int absoluteValueTimeOffset = valueTimeOffset + timeOffset;
		if (absoluteValueTimeOffset == 0) {
			// leave at null
			functionIndexValue = null;
		} else if (absoluteValueTimeOffset < 0) {
			functionIndexValue = ctx.mkSub(timePoint, ctx.mkInt(absoluteValueTimeOffset * -1));
		} else if (absoluteValueTimeOffset > 0) {
			functionIndexValue = ctx.mkAdd(timePoint, ctx.mkInt(absoluteValueTimeOffset));
		}

		Expr functionExpr = ctx.mkApp(valueFunc, functionIndex);
		// determine value expression
		Expr valueExpr = null;
		if (signal.getDatatype().equals(DataType.bool)) {
			valueExpr = ctx.mkBool(Boolean.parseBoolean(value));
		} else {
			// if no number is contained, but the variable name (e.g. if x changes, then...)
			// , then we have to reuse the correct symbol and use the functionIndexValue
			if (value.equals(signal.getName())) {
				valueExpr = ctx.mkApp(valueFunc, functionIndexValue);
			} else if (mathOperator == null) {
				valueExpr = ctx.mkInt(Double.valueOf(value).intValue());
			} else {
				// if there is a math expr, we need to use the functionIndexValue
				// add 20: (= f (- t 1) (+ f (- t 2) 20)
				Expr function2Expr = ctx.mkApp(valueFunc, functionIndexValue);
				if (mathOperator.equals(MathOperator.add)) {
					valueExpr = ctx.mkAdd((ArithExpr) function2Expr, ctx.mkInt(Double.valueOf(value).intValue()));
				} else if (mathOperator.equals(MathOperator.subtract)) {
					valueExpr = ctx.mkSub((ArithExpr) function2Expr, ctx.mkInt(Double.valueOf(value).intValue()));
				} else if (mathOperator.equals(MathOperator.multiply)) {
					valueExpr = ctx.mkMul((ArithExpr) function2Expr, ctx.mkInt(Double.valueOf(value).intValue()));
				} else if (mathOperator.equals(MathOperator.divide)) {
					valueExpr = ctx.mkDiv((ArithExpr) function2Expr, ctx.mkInt(Double.valueOf(value).intValue()));
				}
			}
		}

		BoolExpr valueFuncExpr = null;
		if (isAntecedent && timePoint == outputStateTimePoint) {
			if (orVariationGen == OperatorOrGeneration.normal) {
				// generate =, >, max for >= etc.
				valueFuncExpr = createValueFunc_Combinations(ctx, signal, operator, functionExpr, valueExpr);
			} else if (orVariationGen == OperatorOrGeneration.not) {
				// generate =,>,max,<= for >= etc.
				valueFuncExpr = createValueFunc_Variations(ctx, signal, operator, functionExpr, valueExpr);
			} else {
				// generate only = for =, >= for >= etc.
				valueFuncExpr = createValueFunc(ctx, operator, functionExpr, valueExpr);
			}
		} else {
			// generate only = for =, >= for >= etc.
			valueFuncExpr = createValueFunc(ctx, operator, functionExpr, valueExpr);
		}

		return valueFuncExpr;
	}

	private BoolExpr createValueFunc_Variations(Context ctx, Signal signal, ComparisonOperator operator,
			Expr functionExpr, Expr valueExpr) {
		BoolExpr valueFuncExpr = null;
		if (signal.getDatatype().equals(DataType.bool)) {
			valueFuncExpr = ctx.mkOr(ctx.mkEq(functionExpr, valueExpr), ctx.mkNot(ctx.mkEq(functionExpr, valueExpr)));
		} else {
			if (operator.equals(ComparisonOperator.equal)) {
				valueFuncExpr = ctx.mkOr(ctx.mkEq(functionExpr, valueExpr),
						ctx.mkNot(ctx.mkEq(functionExpr, valueExpr)));
			} else if (operator.equals(ComparisonOperator.greater)) {
				ArithExpr upperLimit = ctx.mkInt(Double.valueOf(signal.getUpperLimit()).intValue());
				BoolExpr equalMax = ctx.mkEq((ArithExpr) functionExpr, upperLimit);
				BoolExpr gt = ctx.mkGt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				BoolExpr lt = ctx.mkLt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				BoolExpr eq = ctx.mkEq((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				valueFuncExpr = ctx.mkOr(gt, equalMax, lt, eq);
			} else if (operator.equals(ComparisonOperator.greaterOrEqual)) {
				ArithExpr upperLimit = ctx.mkInt(Double.valueOf(signal.getUpperLimit()).intValue());
				BoolExpr eq = ctx.mkEq((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				BoolExpr equalMax = ctx.mkEq((ArithExpr) functionExpr, upperLimit);
				BoolExpr gt = ctx.mkGt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				BoolExpr lt = ctx.mkLt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				valueFuncExpr = ctx.mkOr(eq, gt, equalMax, lt);
			} else if (operator.equals(ComparisonOperator.less)) {
				ArithExpr lowerLimit = ctx.mkInt(Double.valueOf(signal.getLowerLimit()).intValue());
				BoolExpr equalMin = ctx.mkEq((ArithExpr) functionExpr, lowerLimit);
				BoolExpr lt = ctx.mkLt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				BoolExpr gt = ctx.mkGt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				BoolExpr eq = ctx.mkEq((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				valueFuncExpr = ctx.mkOr(lt, equalMin, gt, eq);
			} else if (operator.equals(ComparisonOperator.lessOrEqual)) {
				ArithExpr lowerLimit = ctx.mkInt(Double.valueOf(signal.getLowerLimit()).intValue());
				BoolExpr eq = ctx.mkEq((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				BoolExpr equalMin = ctx.mkEq((ArithExpr) functionExpr, lowerLimit);
				BoolExpr lt = ctx.mkLt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				BoolExpr gt = ctx.mkGt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
				valueFuncExpr = ctx.mkOr(eq, lt, equalMin, gt);
			}
		}
		return valueFuncExpr;
	}

	private BoolExpr createValueFunc_Combinations(Context ctx, Signal signal, ComparisonOperator operator,
			Expr functionExpr, Expr valueExpr) {
		BoolExpr valueFuncExpr = null;
		if (operator.equals(ComparisonOperator.equal)) {
			BoolExpr eq = ctx.mkEq(functionExpr, valueExpr);
			valueFuncExpr = eq;
		} else if (operator.equals(ComparisonOperator.greater)) {
			ArithExpr upperLimit = ctx.mkInt(Double.valueOf(signal.getUpperLimit()).intValue());
			BoolExpr equalMax = ctx.mkEq((ArithExpr) functionExpr, upperLimit);
			BoolExpr gt = ctx.mkGt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
			valueFuncExpr = ctx.mkOr(gt, equalMax);
		} else if (operator.equals(ComparisonOperator.greaterOrEqual)) {
			ArithExpr upperLimit = ctx.mkInt(Double.valueOf(signal.getUpperLimit()).intValue());
			BoolExpr eq = ctx.mkEq((ArithExpr) functionExpr, (ArithExpr) valueExpr);
			BoolExpr equalMax = ctx.mkEq((ArithExpr) functionExpr, upperLimit);
			BoolExpr gt = ctx.mkGt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
			valueFuncExpr = ctx.mkOr(eq, gt, equalMax);
		} else if (operator.equals(ComparisonOperator.less)) {
			ArithExpr lowerLimit = ctx.mkInt(Double.valueOf(signal.getLowerLimit()).intValue());
			BoolExpr equalMin = ctx.mkEq((ArithExpr) functionExpr, lowerLimit);
			BoolExpr lt = ctx.mkLt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
			valueFuncExpr = ctx.mkOr(lt, equalMin);
		} else if (operator.equals(ComparisonOperator.lessOrEqual)) {
			ArithExpr lowerLimit = ctx.mkInt(Double.valueOf(signal.getLowerLimit()).intValue());
			BoolExpr eq = ctx.mkEq((ArithExpr) functionExpr, (ArithExpr) valueExpr);
			BoolExpr equalMin = ctx.mkEq((ArithExpr) functionExpr, lowerLimit);
			BoolExpr lt = ctx.mkLt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
			valueFuncExpr = ctx.mkOr(eq, lt, equalMin);
		}
		return valueFuncExpr;
	}

	private BoolExpr createValueFunc(Context ctx, ComparisonOperator operator, Expr functionExpr, Expr valueExpr) {
		BoolExpr valueFuncExpr = null;
		if (operator.equals(ComparisonOperator.equal)) {
			valueFuncExpr = ctx.mkEq(functionExpr, valueExpr);
		} else if (operator.equals(ComparisonOperator.greater)) {
			valueFuncExpr = ctx.mkGt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
		} else if (operator.equals(ComparisonOperator.greaterOrEqual)) {
			valueFuncExpr = ctx.mkGe((ArithExpr) functionExpr, (ArithExpr) valueExpr);
		} else if (operator.equals(ComparisonOperator.less)) {
			valueFuncExpr = ctx.mkLt((ArithExpr) functionExpr, (ArithExpr) valueExpr);
		} else if (operator.equals(ComparisonOperator.lessOrEqual)) {
			valueFuncExpr = ctx.mkLe((ArithExpr) functionExpr, (ArithExpr) valueExpr);
		}
		return valueFuncExpr;
	}

	private class BoolExprTuple {

		public BoolExpr generalExpr_t2;
		private BoolExpr generalExpr;
		private BoolExpr timeExpr;

		public BoolExprTuple(BoolExpr generalExpr, BoolExpr timeExpr, BoolExpr generalExpr_t2) {
			this.generalExpr = generalExpr;
			this.timeExpr = timeExpr;
			this.generalExpr_t2 = generalExpr_t2;
		}

	}

}
