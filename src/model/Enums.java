package model;

public class Enums {

	public enum DataType {
		bool, number, timer;
	}

	public enum Strategy {
		cogCompNLP;
	}

	public enum ComparisonOperator {

		less("<"), lessOrEqual("<="), greater(">"), greaterOrEqual(">="), equal("=");
		public String name;

		private ComparisonOperator(String name) {
			this.name = name;
		}

	}

	public enum MathOperator {

		add("+"), subtract("-"), multiply("*"), divide("/");
		public String name;

		private MathOperator(String name) {
			this.name = name;
		}

	}
}
