package model.expressions;

public class NotExpression extends BoolExpression {

	private BoolExpression expression;

	public NotExpression() {
	}

	public BoolExpression getExpression() {
		return expression;
	}

	public void setExpression(BoolExpression expression) {
		this.expression = expression;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NOT(" + expression.toString() + ")");
		return builder.toString();
	}

}
