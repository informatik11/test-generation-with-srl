package model.expressions;

public class ImplicationExpression extends BoolExpression {

	private BoolExpression antecedent;
	private BoolExpression consequent;

	public ImplicationExpression() {
	}

	public BoolExpression getAntecedent() {
		return antecedent;
	}

	public void setAntecedent(BoolExpression antecedent) {
		this.antecedent = antecedent;
	}

	public BoolExpression getConsequent() {
		return consequent;
	}

	public void setConsequent(BoolExpression consequent) {
		this.consequent = consequent;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		builder.append(antecedent.toString());
		builder.append(" => ");
		builder.append(consequent.toString());
		builder.append(")");
		return builder.toString();
	}

}
