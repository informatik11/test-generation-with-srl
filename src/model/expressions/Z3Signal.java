package model.expressions;

import com.microsoft.z3.FuncDecl;

import model.Signal;

public class Z3Signal {

	private FuncDecl valueFunc;
	private Signal signal;

	public Z3Signal(Signal signal, FuncDecl valueFunc) {
		this.valueFunc = valueFunc;
		this.signal = signal;
	}

	public Signal getSignal() {
		return signal;
	}

	public FuncDecl getValueFunc() {
		return valueFunc;
	}

}
