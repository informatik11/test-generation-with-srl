package model.expressions;

import model.Enums.ComparisonOperator;
import model.Enums.MathOperator;
import model.Signal;

/**
 * contains expression that have exactly one value, a modifier and a signal,
 * e.g. voltage > 80
 * 
 *
 */
public class AtomicExpression extends BoolExpression {

	private Signal signal;
	private ComparisonOperator operator;
	private MathOperator mathOperator;
	private String value;
	private int after = 0;
	private String duration = "";
	private int timeOffset = 0;
	private int valueTimeOffset = 0;

	public AtomicExpression(Signal signal, ComparisonOperator operator, String value, MathOperator mathOperator) {
		setSignal(signal);
		setOperator(operator);
		setValue(value);
		setMathOperator(mathOperator);
	}

	private AtomicExpression(Signal signal, ComparisonOperator operator, String value, MathOperator mathOperator,
			int after, String duration, int timeOffset, int valueTimeOffset) {
		setSignal(signal);
		setOperator(operator);
		setValue(value);
		setMathOperator(mathOperator);
		setAfter(after);
		setDuration(duration);
		setTimeOffset(timeOffset);
		setValueTimeOffset(valueTimeOffset);
	}

	public Signal getSignal() {
		return signal;
	}

	public void setSignal(Signal signal) {
		this.signal = signal;
	}

	public ComparisonOperator getOperator() {
		return operator;
	}

	public void setOperator(ComparisonOperator operator) {
		this.operator = operator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		String op = (operator == null) ? "" : operator.name;
		String after = (this.after == 0) ? "" : " after " + this.after;
		String dur = (this.duration == "") ? "" : " for " + this.duration;
		String offset = (this.timeOffset == 0) ? "" : " @" + String.valueOf(timeOffset);
		if (mathOperator == null)
			builder.append(signal.getName() + " " + op + " " + value + after + dur + offset);
		else {
			builder.append(signal.getName() + " " + op + " " + signal.getName() + "@-1" + " " + mathOperator.name + " "
					+ value + after + dur + offset);
		}
		return builder.toString();
	}

	public int getAfter() {
		return after;
	}

	public void setAfter(int after) {
		this.after = after;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public int getTimeOffset() {
		return timeOffset;
	}

	public void addTimeOffset(int timeOffset) {
		this.timeOffset += timeOffset;
	}

	private void setTimeOffset(int timeOffset) {
		this.timeOffset = timeOffset;
	}

	public MathOperator getMathOperator() {
		return mathOperator;
	}

	public void setMathOperator(MathOperator mathOperator) {
		this.mathOperator = mathOperator;
	}

	public int getValueTimeOffset() {
		return valueTimeOffset;
	}

	public void addValueTimeOffset(int valueTimeOffset) {
		this.valueTimeOffset += valueTimeOffset;
	}

	private void setValueTimeOffset(int valueTimeOffset) {
		this.valueTimeOffset = valueTimeOffset;
	}

}
