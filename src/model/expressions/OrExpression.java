package model.expressions;

import java.util.ArrayList;
import java.util.List;

/**
 * contains expressions that are concatenated with an or
 * 
 *
 */
public class OrExpression extends BoolExpression {

	private List<BoolExpression> expressions;

	public OrExpression() {
		expressions = new ArrayList<BoolExpression>();
	}

	public List<BoolExpression> getExpressions() {
		return expressions;
	}

	public void addExpression(BoolExpression expression) {
		expressions.add(expression);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		if (expressions.size() == 1) {
			builder.append(expressions.get(0).toString());
		} else {
			for (BoolExpression exp : getExpressions()) {
				builder.append(exp.toString() + " OR ");
			}
		}
		builder.append(")");
		return builder.toString();
	}

}
