package model.expressions;

import java.util.ArrayList;
import java.util.List;

public class AndExpression extends BoolExpression {

	private List<BoolExpression> expressions;

	public AndExpression() {
		expressions = new ArrayList<BoolExpression>();
	}

	public List<BoolExpression> getExpressions() {
		return expressions;
	}

	public void addExpression(BoolExpression expression) {
		expressions.add(expression);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		for (BoolExpression exp : getExpressions()) {
			builder.append(exp.toString() + " AND ");
		}
		builder.append(")");
		return builder.toString();
	}

}
