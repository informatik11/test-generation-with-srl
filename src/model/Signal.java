package model;

import java.util.ArrayList;
import java.util.List;

import model.Enums.DataType;

public class Signal {

	private String name;
	private DataType datatype;
	private List<String> possibleValues;
	private String initialValue = "";
	private String upperLimit = "";
	private String lowerLimit = "";

	public Signal(String name, DataType dataType, String initialValue) {
		this.name = name;
		this.datatype = dataType;
		possibleValues = new ArrayList<String>();
		setInitialValue(initialValue);
	}

	public Signal(String name, DataType dataType, String minValue, String maxValue, String initialValue) {
		this.name = name;
		this.datatype = dataType;
		possibleValues = new ArrayList<String>();
		setInitialValue(initialValue);
		lowerLimit = minValue;
		upperLimit = maxValue;
	}

	public String getName() {
		return name;
	}

	public DataType getDatatype() {
		return datatype;
	}

	public List<String> getPossibleValues() {
		return possibleValues;
	}

	public void addPossibleValue(String value) {
		this.possibleValues.add(value);
	}

	public void addPossibleValues(List<String> values) {
		this.possibleValues.addAll(values);
	}

	public String getInitialValue() {
		return initialValue;
	}

	public void setInitialValue(String initialValue) {
		this.initialValue = initialValue;
	}

	public String getUpperLimit() {
		return upperLimit;
	}

	public void setUpperLimit(String upperLimit) {
		this.upperLimit = upperLimit;
	}

	public String getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(String lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	@Override
	public String toString() {
		return name;
	}

}
