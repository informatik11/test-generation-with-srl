package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import model.Enums.ComparisonOperator;
import model.Enums.DataType;
import model.Enums.MathOperator;

public class PredicateFrame {

	private Constituent predicate;
	private LinkedHashMap<String, List<SRLRelation>> relationMap;
	private boolean isCondition = false;

	private ComparisonOperator modifier = ComparisonOperator.equal;
	private List<String> fromVal = new ArrayList<String>();
	private ComparisonOperator fromModifier;
	private String agent = "";
	private MathOperator mathOperator;
	private List<String> toVal = new ArrayList<String>();
	private boolean negated = false;
	private String predicateText;

	private DataType dataType = null;
	boolean isVerbPastTense = false;
	private List<Signal> signalList = new ArrayList<Signal>();

	private List<String> afterVal = new ArrayList<String>();
	private List<String> forVal = new ArrayList<String>();

	private Map<String, TreeSet<Integer>> relationStartSpanMap;
	private Map<String, TreeSet<Integer>> relationEndSpanMap;
	private TreeSet<Integer> signalStartSpans = new TreeSet<Integer>();
	private TreeSet<Integer> signalEndSpans = new TreeSet<Integer>();

	public PredicateFrame(Constituent predicate, String predicateText) {
		this.predicate = predicate;
		this.predicateText = predicateText;
		relationMap = new LinkedHashMap<String, List<SRLRelation>>();
		relationStartSpanMap = new HashMap<String, TreeSet<Integer>>();
		relationEndSpanMap = new HashMap<String, TreeSet<Integer>>();
	}

	public void addRelation(String relationName, String targetName, int startSpan, int endSpan) {
		if (!relationMap.containsKey(relationName)) {
			relationMap.put(relationName, new ArrayList<SRLRelation>());
			relationStartSpanMap.put(relationName, new TreeSet<Integer>());
			relationEndSpanMap.put(relationName, new TreeSet<Integer>());
		}
		SRLRelation relation = new SRLRelation(relationName, targetName, startSpan, endSpan);
		relationMap.get(relationName).add(relation);
		relationStartSpanMap.get(relationName).add(startSpan);
		relationEndSpanMap.get(relationName).add(endSpan);
	}

	/**
	 * returns a map containing the relation name as the key and the target name as
	 * the value
	 * 
	 * @return
	 */
	public Map<String, List<SRLRelation>> getRelationMap() {
		return relationMap;
	}

	/**
	 * returns empty list if no relation found
	 * 
	 * @param relationName
	 * @return
	 */
	public List<SRLRelation> getRelation(String relationName) {
		if (!relationMap.containsKey(relationName))
			return new ArrayList<>();
		else {
			return relationMap.get(relationName);
		}

	}

	public Constituent getPredicate() {
		return predicate;
	}

	public boolean getIsCondition() {
		return isCondition;
	}

	public void setIsCondition(boolean containedInConditionalSentence) {
		isCondition = containedInConditionalSentence;
	}

	public ComparisonOperator getModifier() {
		return modifier;
	}

	public void setModifier(ComparisonOperator mod) {
		this.modifier = mod;
	}

	public List<String> getFromValues() {
		return fromVal;
	}

	public void addFromVal(String condFromVal) {
		if (!this.fromVal.contains(condFromVal) && !condFromVal.isEmpty())
			this.fromVal.add(condFromVal);
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public List<String> getToValues() {
		return toVal;
	}

	public void addToVal(String toVal) {
		if (!this.toVal.contains(toVal))
			this.toVal.add(toVal);
	}

	public DataType getDatatype() {
		return dataType;
	}

	public void setDataType(DataType datatype) {
		this.dataType = datatype;
	}

	public String getPredicateText() {
		return predicateText;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		String pastTense = "";
		if (isVerbPastTense)
			pastTense = " Past";
		else
			pastTense = " Present";
		if (isCondition) {
			builder.append("Cond").append(" verb: " + predicateText + pastTense);

		} else {
			builder.append("Act").append(" verb: " + predicateText + pastTense);
		}
		builder.append("\n   PT: " + signalList);
		builder.append("\n   FV: " + fromVal);
		String mod = (fromModifier == null) ? "" : fromModifier.name();
		builder.append("\n   FM: " + mod);
		builder.append("\n   TOV: " + toVal);
		mod = (modifier == null) ? "" : modifier.name();
		builder.append("\n   TOM: " + mod);
		builder.append("\n   After: " + afterVal);
		builder.append("\n   For: " + forVal);
		String math = (mathOperator == null) ? "" : mathOperator.name();
		builder.append("\n   math: " + math);
		builder.append("\n   neg: " + negated);
		builder.append("\n");
		return builder.toString();
	}

	public void setNegated(boolean negated) {
		this.negated = negated;
	}

	public boolean isNegated() {
		return negated;
	}

	public ComparisonOperator getFromModifier() {
		return fromModifier;
	}

	public void setFromModifier(ComparisonOperator FromModifier) {
		this.fromModifier = FromModifier;
	}

	public List<Signal> getSignals() {
		return signalList;
	}

	public void setVerbPastTense(boolean isPastTense) {
		isVerbPastTense = isPastTense;
	}

	public boolean isVerbPastTense() {
		return isVerbPastTense;
	}

	public void addToValues(List<String> values) {
		for (String value : values) {
			if (!toVal.contains(value)) {
				toVal.add(value);
			}
		}

	}

	public List<String> getAfterVals() {
		return afterVal;
	}

	public void addAfterVals(String afterVal) {
		this.afterVal.add(afterVal);
	}

	public List<String> getForVals() {
		return forVal;
	}

	public void addForVal(String forVal) {
		this.forVal.add(forVal);
	}

	public void addAfterValues(List<String> values) {
		this.afterVal.addAll(values);
	}

	public void addForValues(List<String> values) {
		this.forVal.addAll(values);
	}

	public MathOperator getMathOperator() {
		return mathOperator;
	}

	public void setMathOperator(MathOperator mathOperator) {
		this.mathOperator = mathOperator;
	}

	public int getVerbStartSpan() {
		return predicate.getStartSpan();
	}

	public void addSignal(Signal signal, int startSpan, int endSpan) {
		this.signalList.add(signal);
		this.signalStartSpans.add(startSpan);
		this.signalEndSpans.add(endSpan);
	}

	public int getLowestRelationStartSpan() {
		int lowestStartSpan = 1000;
		for (String relation : relationStartSpanMap.keySet()) {
			if (lowestStartSpan > relationStartSpanMap.get(relation).first()) {
				lowestStartSpan = relationStartSpanMap.get(relation).first();
			}
		}
		return lowestStartSpan;
	}

	public int getHighestRelationEndSpan() {
		int highestEndSpan = 0;
		for (String relation : relationEndSpanMap.keySet()) {
			if (highestEndSpan < relationEndSpanMap.get(relation).last()) {
				highestEndSpan = relationEndSpanMap.get(relation).last();
			}
		}
		return highestEndSpan;
	}

	public int getLowestSignalStartSpan() {
		return signalStartSpans.first();
	}

	public int getHighestSignalEndSpan() {
		return signalEndSpans.last();
	}
}
