package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import model.expressions.AndExpression;
import model.expressions.BoolExpression;

public class Function {

	private String systemName;
	private List<FunctionEntry> functionEntries = new ArrayList<FunctionEntry>();
	private Map<Signal, Integer> signalToTimeStampsMap = new HashMap<Signal, Integer>();

	public Function(String systemName) {
		this.systemName = systemName;
	}

	public String getSystemName() {
		return systemName;
	}

	public void addFunctionEntry(FunctionEntry entry) {
		functionEntries.add(entry);
	}

	public List<FunctionEntry> getFunctionEntries() {
		return functionEntries;
	}

	public boolean containsEntryWithGuards(List<BoolExpression> condGuards) {
		for (FunctionEntry entry : functionEntries) {
			if (equalConditions(entry.getGuards(), condGuards))
				return true;
		}
		return false;
	}

	public void addSignal(Signal signal) {
		if (!signalToTimeStampsMap.containsKey(signal))
			// 2 instead of 1 because the initial value will be added
			signalToTimeStampsMap.put(signal, 2);
	}

	public void addTimestamps(Signal signal, int timeStamps) {
		if (timeStamps > 0)
			signalToTimeStampsMap.put(signal, signalToTimeStampsMap.get(signal) + timeStamps);
	}

	public int getLongestSignalLength() {
		int maxLength = 0;
		for (Signal signal : signalToTimeStampsMap.keySet()) {
			Integer timeStamps = signalToTimeStampsMap.get(signal);
			if (maxLength < timeStamps)
				maxLength = timeStamps;
		}
		return maxLength;
	}

	public Integer getTimeStamps(Signal signal) {
		Integer result = 0;
		if (signalToTimeStampsMap.containsKey(signal)) {
			result = signalToTimeStampsMap.get(signal);
		}
		return result;
	}

	private boolean equalConditions(List<BoolExpression> list, List<BoolExpression> condGuards) {
		// TODO order should not matter but currently does
		return (list.toString().equals(condGuards.toString()));

	}

	// determine if two lists are equal independent of order
	private boolean equalLists(List<String> one, List<String> two) {
		// copy lists so the order in the original is not changed
		one = new ArrayList<String>(one);
		two = new ArrayList<String>(two);

		Collections.sort(one);
		Collections.sort(two);
		return one.equals(two);
	}

	public FunctionEntry getFunctionEntry(List<BoolExpression> condGuards) {
		for (FunctionEntry entry : functionEntries) {
			if (equalConditions(entry.getGuards(), condGuards)) {
				return entry;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(systemName);
		builder.append("\n");
		for (FunctionEntry entry : functionEntries) {
			builder.append(entry.toString());
		}
		builder.append("\n");
		builder.append("Signal Function Size\n");
		for (Signal signal : signalToTimeStampsMap.keySet()) {
			builder.append(signal.getName() + ": " + signalToTimeStampsMap.get(signal) + "\n");
		}
		return builder.toString();
	}

	public BoolExpression generateExpression() {
		AndExpression and = new AndExpression();
		for (FunctionEntry entry : functionEntries) {
			and.addExpression(entry.generateExpression());
		}
		return and;
	}

	/**
	 * the first entry contains the negated antecedentConsequent, the value contains
	 * the whole function (with the negated antecedent and Consequent)
	 * 
	 * @return
	 */
	public LinkedHashMap<BoolExpression, BoolExpression> generateFailExpressions() {

		List<BoolExpression> antecedentNotConsequent = new ArrayList<BoolExpression>();
		List<BoolExpression> antecedentConsequent = new ArrayList<BoolExpression>();
		for (FunctionEntry entry : functionEntries) {
			antecedentNotConsequent.add(entry.generateFailExpression());
			antecedentConsequent.add(entry.generateExpression());
		}
		LinkedHashMap<BoolExpression, BoolExpression> result = new LinkedHashMap<BoolExpression, BoolExpression>();
		for (int i = 0; i < antecedentNotConsequent.size(); i++) {
			AndExpression and = new AndExpression();
			and.addExpression(antecedentNotConsequent.get(i));
			for (int j = 0; j < antecedentConsequent.size(); j++) {
				if (j != i)
					and.addExpression(antecedentConsequent.get(j));
			}
			result.put(antecedentNotConsequent.get(i), and);
		}
		return result;
	}

}
