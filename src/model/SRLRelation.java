package model;

public class SRLRelation {

	private String targetName;
	private String relationName;
	private int startSpan;
	private int endSpan;

	public SRLRelation(String relationName, String targetName, int startSpan, int endSpan) {
		this.targetName = targetName;
		this.relationName = relationName;
		this.startSpan = startSpan;
		this.endSpan = endSpan;
	}

	public String getContent() {
		return targetName;
	}

	public int getStartSpan() {
		return startSpan;
	}

	public int getEndSpan() {
		return endSpan;
	}

	public String getRelationName() {
		return relationName;
	}

	@Override
	public String toString() {
		return targetName;
	}
}
