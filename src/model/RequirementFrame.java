package model;

import java.util.ArrayList;
import java.util.List;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;

public class RequirementFrame {

	private String requirementText;
	private String requirementID;
	private List<PredicateFrame> predicateFrames;
	private int condActionSeparatorIndex;
	private List<Integer> andStartSpans;
	private List<Integer> orStartSpans;

	public RequirementFrame(String id, String text) {
		this.requirementID = id;
		this.requirementText = text;
		predicateFrames = new ArrayList<PredicateFrame>();
		andStartSpans = new ArrayList<Integer>();
		orStartSpans = new ArrayList<Integer>();
	}

	public void addPredicateFrame(String predicate, PredicateFrame predicateFrame) {
		predicateFrames.add(predicateFrame);
	}

	public List<Integer> getAndStartSpans() {
		return andStartSpans;
	}

	public void addAndStartSpan(int startSpan) {
		this.andStartSpans.add(startSpan);
	}

	public void addOrStartSpan(int startSpan) {
		this.orStartSpans.add(startSpan);
	}

	public List<Integer> getOrStartSpans() {
		return orStartSpans;
	}

	public List<PredicateFrame> getPredicateFrames() {
		return predicateFrames;
	}

	public String getRequirementText() {
		return requirementText;
	}

	public String getRequirementID() {
		return requirementID;
	}

	public int getCondActionSeparatorIndex() {
		return condActionSeparatorIndex;
	}

	public List<PredicateFrame> getConditionPredicates() {
		List<PredicateFrame> result = new ArrayList<PredicateFrame>();
		for (PredicateFrame frame : predicateFrames) {
			if (frame.getIsCondition())
				result.add(frame);
		}
		return result;
	}

	public List<PredicateFrame> getActionPredicates() {
		List<PredicateFrame> result = new ArrayList<PredicateFrame>();
		for (PredicateFrame frame : predicateFrames) {
			if (!frame.getIsCondition())
				result.add(frame);
		}
		return result;
	}

	public void setConditionActionSeparator(Constituent marker) {
		this.condActionSeparatorIndex = marker.getStartSpan();
	}

	public boolean signalIsContainedInActions(Signal signal) {
		List<PredicateFrame> actionFrames = getActionPredicates();
		for (PredicateFrame predFrame : actionFrames) {
			for (Signal signal2 : predFrame.getSignals()) {
				if (signal2.equals(signal))
					return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(requirementID + ": " + requirementText + "\n");
		for (PredicateFrame predicate : predicateFrames) {
			builder.append(predicate.toString());
			builder.append("\n");
		}
		return builder.toString();
	}

	public boolean signalIsContainedInConditions(Signal signal) {
		List<PredicateFrame> condFrames = getConditionPredicates();
		for (PredicateFrame predFrame : condFrames) {
			for (Signal signal2 : predFrame.getSignals()) {
				if (signal2.equals(signal))
					return true;
			}
		}
		return false;
	}

	public boolean signalIsContainedInConditions(List<Signal> signalList) {
		List<PredicateFrame> condFrames = getConditionPredicates();
		for (PredicateFrame predFrame : condFrames) {
			for (Signal signal : predFrame.getSignals()) {
				for (Signal signal2 : signalList) {
					if (signal.equals(signal2))
						return true;
				}
			}
		}
		return false;
	}

	public boolean signalIsContainedInActions(List<Signal> signalList) {
		List<PredicateFrame> actFrames = getActionPredicates();
		for (PredicateFrame predFrame : actFrames) {
			for (Signal signal : predFrame.getSignals()) {
				for (Signal signal2 : signalList) {
					if (signal.equals(signal2))
						return true;
				}
			}
		}
		return false;
	}

}
