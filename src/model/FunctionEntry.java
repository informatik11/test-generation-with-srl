package model;

import java.util.ArrayList;
import java.util.List;

import model.expressions.AndExpression;
import model.expressions.BoolExpression;
import model.expressions.ImplicationExpression;
import model.expressions.NotExpression;

public class FunctionEntry {

	private List<BoolExpression> actions;
	private List<BoolExpression> guards;

	public FunctionEntry() {
		this.guards = new ArrayList<BoolExpression>();
		this.actions = new ArrayList<BoolExpression>();
	}

	public FunctionEntry(List<BoolExpression> condGuards) {
		this.guards = condGuards;
		this.actions = new ArrayList<BoolExpression>();
	}

	public void addGuard(BoolExpression guard) {
		guards.add(guard);
	}

	public void addAction(BoolExpression action) {
		this.actions.add(action);
	}

	public List<BoolExpression> getGuards() {
		return guards;
	}

	public void addActions(List<BoolExpression> actions) {
		this.actions.addAll(actions);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Guards:");
		builder.append("\n");
		for (BoolExpression guard : guards) {
			builder.append(guard.toString());
			builder.append("\n");
		}
		builder.append("Actions:");
		builder.append("\n");
		for (BoolExpression action : actions) {
			builder.append(action.toString());
			builder.append("\n");
		}
		return builder.toString();
	}

	public BoolExpression generateExpression() {
		ImplicationExpression impl = new ImplicationExpression();

		AndExpression and = new AndExpression();
		for (BoolExpression exp : guards) {
			and.addExpression(exp);
		}

		AndExpression and2 = new AndExpression();
		for (BoolExpression exp : actions) {
			and2.addExpression(exp);
		}

		impl.setAntecedent(and);
		impl.setConsequent(and2);
		return impl;
	}

	public BoolExpression generateFailExpression() {
		ImplicationExpression impl = new ImplicationExpression();

		AndExpression and = new AndExpression();
		for (BoolExpression exp : guards) {
			and.addExpression(exp);
		}

		AndExpression and2 = new AndExpression();
		for (BoolExpression exp : actions) {
			and2.addExpression(exp);
		}
		NotExpression not = new NotExpression();
		not.setExpression(and2);

		impl.setAntecedent(and);
		impl.setConsequent(not);
		return impl;
	}

}
