package model;

import model.Enums.DataType;

public class Input extends Signal {

	
	public Input(String name, DataType dataType, String initialValue) {
		super(name, dataType, initialValue);
	}
	public Input(String name, DataType dataType, String minValue, String maxValue, String initialValue) {
		super(name, dataType, minValue, maxValue, initialValue);
	}

}
