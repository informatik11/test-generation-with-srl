package model.testgeneration;

import java.util.ArrayList;
import java.util.List;

public class TestSuite {

	private List<TestCase> testCases;
	private String systemName;

	public TestSuite(String systemName) {
		this.systemName = systemName;
		testCases = new ArrayList<TestCase>();
	}

	public List<TestCase> getTestCases() {
		return testCases;
	}

	public String getName() {
		return systemName;
	}

	public void addTestCase(TestCase testCase) {
		this.testCases.add(testCase);
	}

	public void addTestCases(List<TestCase> testCases) {
		this.testCases.addAll(testCases);
	}

}
