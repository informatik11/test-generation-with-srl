package model.testgeneration;

import java.util.SortedMap;
import java.util.TreeMap;

import model.Signal;

public class TestSignal {

	private Signal signal;
	private SortedMap<Integer, String> timeValueMap;

	public TestSignal(Signal signal) {
		this.signal = signal;
		timeValueMap = new TreeMap<Integer, String>();
	}

	public void addTimeValuePair(Integer timeStamp, String value) {
		timeValueMap.put(timeStamp, value);
	}

	public SortedMap<Integer, String> getTimeValueMap() {
		return timeValueMap;
	}

	public Signal getSignal() {
		return signal;
	}

}
