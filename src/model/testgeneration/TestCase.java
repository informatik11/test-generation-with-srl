package model.testgeneration;

import java.util.ArrayList;
import java.util.List;

public class TestCase {

	private List<TestSignal> signalList;
	private String name;
	private boolean shouldFail = false;

	public TestCase(String name) {
		this.name = name;
		signalList = new ArrayList<TestSignal>();
	}

	public List<TestSignal> getSignalList() {
		return signalList;
	}

	public void addSignal(TestSignal signal) {
		this.signalList.add(signal);
	}

	public String getName() {
		return name;
	}

	public boolean shouldFail() {
		return shouldFail;
	}

	public void setShouldFail(boolean shouldFail) {
		this.shouldFail = shouldFail;
	}
}
