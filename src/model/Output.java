package model;

import model.Enums.DataType;

public class Output extends Signal {

	public Output(String name, DataType dataType, String initialValue) {
		super(name, dataType, initialValue);
	}

	public Output(String name, DataType dataType, String minValue, String maxValue, String initialValue) {
		super(name, dataType, minValue, maxValue, initialValue);
	}

}
