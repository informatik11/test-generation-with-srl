package testgeneration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import model.Enums.ComparisonOperator;
import model.Enums.MathOperator;
import model.Function;
import model.FunctionEntry;
import model.Input;
import model.Output;
import model.PredicateFrame;
import model.RequirementFrame;
import model.Signal;
import model.expressions.AndExpression;
import model.expressions.AtomicExpression;
import model.expressions.BoolExpression;
import model.expressions.NotExpression;
import model.expressions.OrExpression;
import slr.SRLData;

public class FunctionGenerator {

	private Map<Signal, Boolean> additionalTimeStampForReqFrameAlreadyAddedMap;

	public void generateFunctions(String systemName, Map<String, String> idToRequirements, SRLData data,
			List<Input> inputs, List<Output> outputs) {

		List<Signal> signals = new ArrayList<Signal>();
		signals.addAll(inputs);
		signals.addAll(outputs);
		additionalTimeStampForReqFrameAlreadyAddedMap = new HashMap<Signal, Boolean>();
		for (RequirementFrame reqFrame : data.getRequirementFrames()) {
			// reset timeStampForReqFrameAlreadyAdded Map for each req Frame
			for (Signal signal : signals) {
				additionalTimeStampForReqFrameAlreadyAddedMap.put(signal, false);
			}
			if (!data.getFunctionMap().containsKey(systemName)) {
				data.getFunctionMap().put(systemName, new Function(systemName));
			}
			Function function = data.getFunctionMap().get(systemName);

			LinkedList<BoolExpression> condGuards = new LinkedList<BoolExpression>();
			LinkedList<BoolExpression> actions = new LinkedList<BoolExpression>();

			// generate conditions
			List<PredicateFrame> frames = reqFrame.getConditionPredicates();
			// sort according to start span
			Collections.sort(frames, new PredicateComparator());

			for (int i = 0; i < frames.size(); i++) {
				PredicateFrame frame = frames.get(i);
				boolean isClosedLoopVariable = false;
				if (reqFrame.signalIsContainedInActions(frame.getSignals())) {
					isClosedLoopVariable = true;

				}
				BoolExpression expression = generateConditionExpression(frame, function, isClosedLoopVariable,
						reqFrame);

				determineOrAndConnection(frame, condGuards, frames, i, expression, reqFrame);

			}
			// generate actions
			List<PredicateFrame> actFrames = reqFrame.getActionPredicates();
			// sort according to start span
			Collections.sort(actFrames, new PredicateComparator());
			for (int i = 0; i < actFrames.size(); i++) {
				PredicateFrame predFrame = actFrames.get(i);
				boolean isClosedLoopVariable = false;
				if (reqFrame.signalIsContainedInConditions(predFrame.getSignals())) {
					isClosedLoopVariable = true;
				}
				BoolExpression expression = generateActionExpression(predFrame, function, isClosedLoopVariable,
						reqFrame);

				determineOrAndConnection(predFrame, actions, actFrames, i, expression, reqFrame);
			}

			if (!function.containsEntryWithGuards(condGuards)) {
				function.addFunctionEntry(new FunctionEntry(condGuards));
			}
			FunctionEntry entry = function.getFunctionEntry(condGuards);
			entry.addActions(actions);

		}
		for (String functionName : data.getFunctionMap().keySet()) {
			Function function = data.getFunctionMap().get(functionName);
			// check if a timestamp needs to be added for signal x: it might be a closed
			// loop variable but not within one req. frame, e.g. if x then y, if y then x
			for (Signal signal : signals) {
				boolean signalContainedInAction = false;
				boolean signalContainedInCondition = false;
				for (RequirementFrame reqFrame : data.getRequirementFrames()) {
					// in the case that it is contained in both, a time stamp has already been added
					// as it was a closed loop variable
					if (reqFrame.signalIsContainedInActions(signal) ^ reqFrame.signalIsContainedInConditions(signal)) {
						if (reqFrame.signalIsContainedInActions(signal))
							signalContainedInAction = true;
						if (reqFrame.signalIsContainedInConditions(signal))
							signalContainedInCondition = true;
					}
				}
				if (signalContainedInAction && signalContainedInCondition)
					function.addTimestamps(signal, 1);
			}
		}

		// DEBUG
		for (String functionName : data.getFunctionMap().keySet()) {
			Function function = data.getFunctionMap().get(functionName);
			System.out.println(function.toString());
		}
	}

	private void determineOrAndConnection(PredicateFrame currentFrame, LinkedList<BoolExpression> expressionList,
			List<PredicateFrame> frames, int i, BoolExpression currentExpression, RequirementFrame reqFrame) {
		// determine whether this condition is connected via an "or" or "and" with the
		// previous one
		boolean connectedViaAnd = false;
		if (i > 0) {
			PredicateFrame frame1 = frames.get(i - 1);
			int beginSpan = frame1.getHighestRelationEndSpan();
			int endSpan = currentFrame.getLowestRelationStartSpan();
			List<Integer> andStartSpans = reqFrame.getAndStartSpans();
			List<Integer> orStartSpans = reqFrame.getOrStartSpans();
			connectedViaAnd = isConnectedViaAnd(beginSpan, endSpan, andStartSpans, orStartSpans);
		} else {
			connectedViaAnd = true;
		}
		if (connectedViaAnd) {
			expressionList.add(currentExpression);
		} else {
			BoolExpression lastCondition = expressionList.removeLast();
			OrExpression or = new OrExpression();
			or.addExpression(lastCondition);
			or.addExpression(currentExpression);
			expressionList.add(or);
		}
	}

	private boolean isConnectedViaAnd(int beginSpan, int endSpan, List<Integer> andStartSpans,
			List<Integer> orStartSpans) {
		boolean connectedViaAnd = false;
		// check whether a CC tag in the POS View is between both frames:
		// as the start point, use the endSpan of the previous frame (as they are
		// ordered)
		// and as the end point use the startSpan of the current frame
		// check whether it is "and", if there is no and, check for "or" and if there is
		// no "or", assume "and"
		// checking only for "and" will yield false negatives, e.g. assign x to y, z to
		// a etc. , so we need to check for "or" as well
		for (Integer andStartSpan : andStartSpans) {
			if (andStartSpan > beginSpan && andStartSpan < endSpan) {
				connectedViaAnd = true;
			}
		}
		if (!connectedViaAnd) {
			// check if or is between begin and end span
			boolean orFound = false;
			for (Integer orStartSpan : orStartSpans) {
				if (orStartSpan > beginSpan && orStartSpan < endSpan) {
					orFound = true;
				}
			}
			if (!orFound)
				connectedViaAnd = true;
			else
				connectedViaAnd = false;
		}
		return connectedViaAnd;
	}

	private BoolExpression generateActionExpression(PredicateFrame predFrame, Function function,
			boolean isClosedLoopVariable, RequirementFrame reqFrame) {
		// TODO current limitation: variables are not supported as values for
		// afterValues, toValues etc.
		List<Signal> signals = predFrame.getSignals();
		List<String> values = new ArrayList<String>();
		ComparisonOperator operator = predFrame.getModifier();
		MathOperator mathOperator = predFrame.getMathOperator();
		boolean isPastTense = predFrame.isVerbPastTense();
		String verb = predFrame.getPredicateText();
		List<BoolExpression> expForSignals = new ArrayList<BoolExpression>();
		// for each signal, generate atomic expressions for each value
		for (Signal signal : signals) {
			function.addSignal(signal);

			if (verb.equals("reset")) {
				// reset the variable to its initial value
				values.add(signal.getInitialValue());
			} else {
				values = predFrame.getToValues();
			}
			OrExpression or = new OrExpression();
			BoolExpression signalExpr;
			List<String> afterValues = predFrame.getAfterVals();
			int afterValue = 0;
			if (!afterValues.isEmpty())
				afterValue = Double.valueOf(afterValues.get(0)).intValue();

			for (String value : values) {
				AtomicExpression atom = new AtomicExpression(signal, operator, value, mathOperator);
				if (mathOperator != null && !additionalTimeStampForReqFrameAlreadyAddedMap.get(signal)) {
					function.addTimestamps(signal, 1);
					additionalTimeStampForReqFrameAlreadyAddedMap.put(signal, true);
				}
				if (mathOperator != null)
					atom.addValueTimeOffset(-1);

				function.addTimestamps(signal, afterValue - 1);
				atom.setAfter(afterValue);
				int timeOffset = calculateActionTimeOffset(verb, isClosedLoopVariable, isPastTense);
				atom.addTimeOffset(timeOffset);
				if (verb.contentEquals("switch")) {
					AndExpression andExp = new AndExpression();
					andExp.addExpression(atom);
					andExp.addExpression(
							createSwitchExpr(predFrame, signal, operator, value, mathOperator, timeOffset));
					function.addTimestamps(signal, 1);
					or.addExpression(andExp);
				} else {
					or.addExpression(atom);
				}
			}
			signalExpr = or;
			if (values.isEmpty() && verb.equals("change")) {
				// x != x @ -1, modeled with after -1
				AtomicExpression atom = new AtomicExpression(signal, operator, signal.getName(), mathOperator);
				atom.addValueTimeOffset(-1);
				if (mathOperator != null && !additionalTimeStampForReqFrameAlreadyAddedMap.get(signal)) {
					function.addTimestamps(signal, 1);
					additionalTimeStampForReqFrameAlreadyAddedMap.put(signal, true);
				}
				int timeOffset = calculateActionTimeOffset(verb, isClosedLoopVariable, isPastTense);
				atom.setAfter(afterValue);
				function.addTimestamps(signal, afterValue - 1);
				atom.addTimeOffset(timeOffset);
				NotExpression not = new NotExpression();
				not.setExpression(atom);
				signalExpr = not;
			}
			expForSignals.add(signalExpr);
		}
		BoolExpression result;
		if (expForSignals.size() > 1) {
			// determine connection
			// TODO pairwise connection instead of assuming one type of connection is valid
			// for all signals
			// TODO to be more precise, look at the semantic roles instead of taking the
			// spans from the complete frame
			int beginSpan = predFrame.getLowestSignalStartSpan();
			int endSpan = predFrame.getHighestSignalEndSpan();
			boolean isConnectedViaAnd = isConnectedViaAnd(beginSpan, endSpan, reqFrame.getAndStartSpans(),
					reqFrame.getOrStartSpans());
			if (isConnectedViaAnd) {
				AndExpression and = new AndExpression();
				for (BoolExpression exp : expForSignals) {
					and.addExpression(exp);
				}
				result = and;
			} else {
				OrExpression or = new OrExpression();
				for (BoolExpression exp : expForSignals) {
					or.addExpression(exp);
				}
				result = or;
			}
		} else {
			result = expForSignals.get(0);
		}
		BoolExpression finalResult = result;
		if (predFrame.isNegated()) {
			NotExpression not = new NotExpression();
			not.setExpression(result);
			finalResult = not;
		}

		return finalResult;
	}

	private BoolExpression createSwitchExpr(PredicateFrame predFrame, Signal signal, ComparisonOperator operator,
			String value, MathOperator mathOperator, int timeOffset) {
		if (predFrame.getFromValues().isEmpty()) {
			AtomicExpression atom = new AtomicExpression(signal, operator, value, mathOperator);
			NotExpression not = new NotExpression();
			not.setExpression(atom);
			// set timeoffset to the normal one - 1
			atom.addTimeOffset(timeOffset - 1);
			return not;
		} else {
			// TODO currently only supports a single fromValue
			AtomicExpression atom = new AtomicExpression(signal, predFrame.getFromModifier(),
					predFrame.getFromValues().get(0), mathOperator);
			// set timeoffset to the normal one - 1
			atom.addTimeOffset(timeOffset - 1);
			return atom;
		}
	}

	private int calculateActionTimeOffset(String verb, boolean isClosedLoopVariable, boolean isPastTense) {
		int timeOffset = 0;
		if (isPastTense)
			timeOffset = timeOffset - 1;
		return timeOffset;
	}

	private int calculateConditionTimeOffset(String verb, boolean isClosedLoopVariable, boolean isPastTense) {
		int timeOffset = 0;
		if (isPastTense)
			timeOffset = timeOffset - 1;
		// special case for "changes"
		if (isClosedLoopVariable && !(verb.equals("change")))
			timeOffset = timeOffset - 1;
		return timeOffset;
	}

	private BoolExpression generateConditionExpression(PredicateFrame predFrame, Function function,
			boolean isClosedLoopVariable, RequirementFrame reqFrame) {
		List<Signal> signals = predFrame.getSignals();
		List<String> values = predFrame.getToValues();
		ComparisonOperator operator = predFrame.getModifier();
		BoolExpression condExpression;
		String verb = predFrame.getPredicateText();
		MathOperator mathOperator = predFrame.getMathOperator();
		List<BoolExpression> expForSignals = new ArrayList<BoolExpression>();
		boolean isPastTense = predFrame.isVerbPastTense();
		int timeOffset = 0;
		for (Signal signal : signals) {
			function.addSignal(signal);
			OrExpression or = new OrExpression();
			List<String> forValues = predFrame.getForVals();
			List<String> afterValues = predFrame.getAfterVals();
			int afterValue = 0;
			if (!afterValues.isEmpty())
				afterValue = Double.valueOf(afterValues.get(0)).intValue();
			for (String value : values) {
				AtomicExpression atom = new AtomicExpression(signal, operator, value, mathOperator);
				if ((predFrame.isVerbPastTense() || isClosedLoopVariable || mathOperator != null)
						&& !additionalTimeStampForReqFrameAlreadyAddedMap.get(signal)) {
					function.addTimestamps(signal, 1);
					additionalTimeStampForReqFrameAlreadyAddedMap.put(signal, true);
				}
				if (mathOperator != null)
					atom.addValueTimeOffset(-1);
				timeOffset = calculateConditionTimeOffset(verb, isClosedLoopVariable, isPastTense);
				atom.setAfter(afterValue);
				function.addTimestamps(signal, afterValue - 1);
				atom.addTimeOffset(timeOffset);
				if (verb.contentEquals("switch")) {
					AndExpression andExp = new AndExpression();
					andExp.addExpression(atom);
					andExp.addExpression(
							createSwitchExpr(predFrame, signal, operator, value, mathOperator, timeOffset));
					function.addTimestamps(signal, 1);
					or.addExpression(andExp);
				} else {
					or.addExpression(atom);
				}
			}
			condExpression = or;

			if (values.isEmpty() && verb.equals("change")) {
				// x != x @ -1, modeled with after -1
				AtomicExpression atom = new AtomicExpression(signal, operator, signal.getName(), mathOperator);

				if ((predFrame.isVerbPastTense() || isClosedLoopVariable || mathOperator != null)
						&& !additionalTimeStampForReqFrameAlreadyAddedMap.get(signal)) {
					function.addTimestamps(signal, 1);
					additionalTimeStampForReqFrameAlreadyAddedMap.put(signal, true);
				}
				atom.addValueTimeOffset(-1);
				timeOffset = calculateConditionTimeOffset(verb, isClosedLoopVariable, isPastTense);
				function.addTimestamps(signal, afterValue - 1);
				atom.setAfter(afterValue);
				atom.addTimeOffset(timeOffset);
				NotExpression not = new NotExpression();
				not.setExpression(atom);
				condExpression = not;
			}
			expForSignals.add(condExpression);
		}

		BoolExpression result;
		if (expForSignals.size() > 1) {
			// determine connection
			// TODO pairwise connection instead of assuming one type of connection is valid
			// for all signals
			// TODO to be more precise, look at the semantic roles instead of taking the
			// spans from the complete frame
			int beginSpan = predFrame.getLowestSignalStartSpan();
			int endSpan = predFrame.getHighestSignalEndSpan();
			boolean isConnectedViaAnd = isConnectedViaAnd(beginSpan, endSpan, reqFrame.getAndStartSpans(),
					reqFrame.getOrStartSpans());
			if (isConnectedViaAnd) {
				AndExpression and = new AndExpression();
				for (BoolExpression exp : expForSignals) {
					and.addExpression(exp);
				}
				result = and;
			} else {
				OrExpression or = new OrExpression();
				for (BoolExpression exp : expForSignals) {
					or.addExpression(exp);
				}
				result = or;
			}
		} else {
			result = expForSignals.get(0);
		}
		BoolExpression finalResult = result;
		if (predFrame.isNegated()) {
			NotExpression not = new NotExpression();
			not.setExpression(result);
			finalResult = not;
		}
		return finalResult;
	}

	private class PredicateComparator implements Comparator<PredicateFrame> {
		@Override
		public int compare(PredicateFrame frame1, PredicateFrame frame2) {
			int startSpan1 = frame1.getVerbStartSpan();
			int startSpan2 = frame2.getVerbStartSpan();
			if (startSpan1 < startSpan2)
				return -1;
			else if (startSpan1 > startSpan2)
				return 1;
			else
				return 0;
		}
	}

}
