package testgeneration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Enums.DataType;
import model.Input;
import model.Output;

public class Main {

	private static Map<String, String> idToRequirements = new HashMap<String, String>();
	private static List<Input> inputs = new ArrayList<Input>();
	private static List<Output> outputs = new ArrayList<Output>();
	private static String systemName;
	private static int estimationToReachOutputState = 1;

	public static void main(String[] args) throws IOException {

		TIS_Nat2Test();
		// vendingMachineNat2Test();
		// windowControl();

		// start test generation
		TestGenerator generator = new TestGenerator();
		generator.generateTests(systemName, idToRequirements, inputs, outputs, estimationToReachOutputState);
	}

	private static void testTimeConcat() {
		String text = "When x is 1, switch x to 10 after 3 seconds.";
		String text2 = "When x is 10, switch x to 20 after 2 seconds.";

		outputs.add(new Output("x", DataType.number, "0", "225", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "TimeConcatTest";
	}

	private static void testTimeConcat3() {
		String text = "When x is 1, then the system shall assign 10 to y after 3 seconds.";
		String text2 = "When x is 10, then the system shall assign 20 to y after 2 seconds.";

		outputs.add(new Output("x", DataType.number, "0", "225", "1"));
		outputs.add(new Output("y", DataType.number, "0", "225", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "TimeConcatTest";
	}

	private static void testTimeConcat4() {
		String text = "When x is 2, then the system shall assign 10 to y after 3 seconds.";
		String text2 = "When x1 is 2, then the system shall assign 10 to y after 2 seconds.";

		outputs.add(new Output("x", DataType.number, "0", "225", "1"));
		outputs.add(new Output("x1", DataType.number, "0", "225", "1"));
		outputs.add(new Output("y", DataType.number, "0", "225", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "TimeConcatTest";
	}

	private static void testTimeConcat2() {
		String text = "When x is 1 and y is 1, then switch x to 10 after 3 seconds and switch y to 100 after 50 seconds.";
		String text2 = "When x is 10, switch x to 20 after 2 seconds.";
		String text3 = "When y is 100, switch y to 40 after 5 seconds.";
		String text4 = "When y is 40, switch y to 45 after 4 seconds.";

		outputs.add(new Output("x", DataType.number, "0", "225", "2"));
		outputs.add(new Output("y", DataType.number, "0", "225", "2"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		idToRequirements.put("3", text3);
		idToRequirements.put("4", text4);
		systemName = "TimeConcatTest2";
	}

	private static void windowControl() {
		List<String> reqList = new ArrayList<String>();
		reqList.add(
				"When driver_up, driver_down, passenger_up and passenger_down is false, the system shall set move_up to false, assign false to move_down and assign 0 to error_counter.");
		// error_counter
		reqList.add(
				"When driver_up and driver_down is true and passenger_up and passenger_down is true, then the system shall set error_counter to 2.");
		reqList.add(
				"When driver_up and driver_down is true and passenger_up or passenger_down is false, then the system shall set error_counter to 1.");
		reqList.add(
				"When passenger_up and passenger_down is true and driver_up or driver_down is false, then the system shall set error_counter to 1.");
		reqList.add(
				"When driver_up or driver_down is false and passenger_up or passenger_down is false, then the system shall set error_counter to 0.");
		reqList.add(
				"When error_counter is 0, then driver_up or driver_down is false and passenger_up or passenger_down is false.");
		reqList.add(
				"When error_counter is greater than 0, then driver_up and driver_down is true or passenger_up and passenger_down is true.");
		// moveup
		reqList.add(
				"When driver_up is true and error_counter is 0 and obstacle_detection is false, then the system shall assign true to move_up.");
		reqList.add(
				"When passenger_up is true and driver_down is false and error_counter is 0 and obstacle_detection is false, then the system shall assign true to move_up.");
		reqList.add(
				"When move_up is true, then driver_up or passenger_up is true and error_counter is 0 and obstacle_detection is false.");

		// movedown
		reqList.add("When driver_down is true and error_counter is 0, then the system shall assign true to move_down.");
		reqList.add(
				"When passenger_down is true and driver_up is false and error_counter is 0, then the system shall assign true to move_down.");
		reqList.add(
				"When move_down is true, then driver_down or passenger_down is true and driver_up is false and error_counter is 0.");
		// move down and move up shall not occur simmultaneously
		reqList.add("When move_up is true, then move_down is false.");
		reqList.add("When move_down is true, then move_up is false.");
		// driver takes priority over passenger
		reqList.add(
				"When passenger_up is true and driver_down is true and error_counter is 0, then move_down is true.");
		reqList.add(
				"When passenger_down is true and driver_up is true and error_counter is 0 and obstacle_detection is false, then move_up is true.");
		// if there is an error, neither move up nor down shall be true
		reqList.add("When error_counter is greater than 0, then move_down is false and move_up is false.");
		// driver_info
		reqList.add(
				"When error_counter is greater or equal to 1, then the system shall assign 2 to driver_information.");
		reqList.add(
				"When error_counter is 0 and the vehicle_speed is less or equal to 30, then the system shall assign 0 to driver_information.");
		reqList.add(
				"When error_counter is 0, and the vehicle_speed is greater than 30, and current_position is less than 100, then the system shall assign 1 to driver_information.");
		reqList.add("When driver_information is 2, then error_counter is greater or equal to 1.");
		reqList.add(
				"When driver_information is 1, then the vehicle_speed is greater than 30, and current_position is less than 100, and the error_counter is 0.");
		reqList.add(
				"When driver_information is 0, then error_counter is 0 and the vehicle_speed is less or equal to 30 or current_position is 100.");
		// obstacle detection
		reqList.add("When obstacle_detection is true, then the system shall set move_up to false.");
		// current position
		reqList.add(
				" When current_position is less than 100 and move_up is true, then the system shall add 1 to current_position.");
		reqList.add("When move_up is true and current_position is 100, then current_position is 100.");
		reqList.add(
				"When move_down is true and current_position is higher than 0, then the system shall subtract 1 from current_position.");
		reqList.add("When move_down is true and current_position is 0, then current_position is 0.");
		reqList.add("When current_position changes its value, then move_up is true, or move_down is true.");
		reqList.add(
				"When current_position changes its value, then the system shall subtract 1 from current_position or it shall add 1 to current_position.");

		inputs.add(new Input("driver_up", DataType.bool, "false"));
		inputs.add(new Input("driver_down", DataType.bool, "false"));
		inputs.add(new Input("passenger_up", DataType.bool, "false"));
		inputs.add(new Input("passenger_down", DataType.bool, "false"));
		inputs.add(new Input("obstacle_detection", DataType.bool, "false"));
		inputs.add(new Input("vehicle_speed", DataType.number, "0", "300", "20"));

		outputs.add(new Output("move_up", DataType.bool, "false"));
		outputs.add(new Output("move_down", DataType.bool, "false"));
		outputs.add(new Output("error_counter", DataType.number, "0", "2", "0"));
		outputs.add(new Output("current_position", DataType.number, "0", "100", "0"));
		outputs.add(new Output("driver_information", DataType.number, "0", "2", "0"));
		estimationToReachOutputState = 152;
		for (int i = 0; i < reqList.size(); i++) {
			idToRequirements.put(String.valueOf(i), reqList.get(i));
		}

		systemName = "WindowControl";
	}

	private static void vendingMachineNat2Test() {
		List<String> reqList = new ArrayList<String>();
		// mode
		// 0: choice
		// 1: idle
		// 2: preparing strong coffee
		// 3: preparing weak coffee
		reqList.add(
				"When the system mode is 1, and the coin sensor is on, and the coin sensor was off, the coffee machine system shall set the system mode to 0 and reset the request timer.");
		reqList.add(
				"When the system mode is 0, and the coffee request sensor is on, and the coffee request sensor was off, and the coin sensor is off, and the coin sensor was off, and the request timer is lower or equal to 30, the coffee machine system shall set the system mode to 3 and reset the request timer.");
		reqList.add(
				"When the system mode is 0, and the coffee request sensor is on, and the coffee request sensor was off, and the coin sensor is off, and the coin sensor was off, and the request timer is greater than 30, the coffee machine system shall set the system mode to 2 and reset the request timer.");
		reqList.add(
				"When the system mode is 3, and the request timer was greater or equal to 10, and the request timer was lower or equal to 30, the coffee machine system shall set the system mode to 1 and it shall set the coffee machine output to 1.");
		reqList.add(
				"When the system mode is 2, and the request timer was greater or equal to 30, and the request timer was lower or equal to 50, the coffee machine system shall set the system mode to 1 and it shall set the coffee machine output to 0.");

		inputs.add(new Input("the coin sensor", DataType.bool, "false"));
		inputs.add(new Input("the coffee request sensor", DataType.bool, "false"));

		outputs.add(new Output("the request timer", DataType.timer, "-1", "60", "0"));
		outputs.add(new Output("the system mode", DataType.number, "0", "3", "1"));
		outputs.add(new Output("the coffee machine output", DataType.number, "0", "1", "0"));

		estimationToReachOutputState = 150;
		for (int i = 0; i < reqList.size(); i++) {
			idToRequirements.put(String.valueOf(i), reqList.get(i));
		}
		systemName = "VendingMachine";
	}

	private static void TIS_Nat2Test() {
		List<String> reqList = new ArrayList<String>();
		reqList.add(
				"When the voltage was greater than 80 and the voltage is lower than or equal to 80, the lights controller component shall assign 2 to the indication lights and reset the timer.");
		reqList.add(
				"When the voltage switches to greater than 80 or the mode switched to 1, and the voltage is greater than 80, and the mode was 1, the lights controller component shall assign 1 to the indication lights and reset the timer.");
		reqList.add(
				"When the voltage switches to greater than 80 or the mode switched to 3, and the voltage is greater than 80, and the mode was 3, the lights controller component shall assign 3 to the indication lights and reset the timer.");
		reqList.add(
				"When the voltage switches to greater than 80 or the mode switched to 0, and the voltage is greater than 80, and the mode was 0, the lights controller component shall assign 0 to the indication lights and reset the timer.");
		reqList.add(
				"When the voltage is greater than 80, and the mode was 2, the lights controller component shall assign 2 to the indication lights and reset the timer.");
		reqList.add(
				"When the voltage is greater than 80, and the timer is greater or equal to 34, and the indication lights are 1 or 3, the lights controller component shall assign 2 to the indication lights and reset the timer.");
		reqList.add(
				"When the voltage is greater than 80, and the timer is greater or equal to 22, and the indication lights are 2, and the mode was 1, the lights controller component shall assign 1 to the indication lights and reset the timer.");
		reqList.add(
				"When the voltage is greater than 80, and the timer is greater or equal to 22, and the indication lights are 2, and the mode was 3, the lights controller component shall assign 3 to the indication lights and reset the timer.");
		reqList.add(
				"When the voltage is greater than 80, and the timer is greater or equal to 22, and the indication lights are 2, and the mode was 0, the lights controller component shall assign 0 to the indication lights and reset the timer.");

		reqList.add(
				"When the turn indicator lever switches to 2, and the emergency mode is off, the system shall assign 3 to the mode and reset the timer.");
		reqList.add(
				"When the turn indicator lever switches to 1, and the emergency mode is off, the system shall assign 1 to the mode and reset the timer.");
		reqList.add("When emergency mode switches to true,  the system shall set the mode to 0 and reset the timer.");
		reqList.add(
				"When emergency mode is on, and emergency mode was on, and the turn indicator lever switches to 1, the system shall assign 1 to the mode and reset the timer.");
		reqList.add(
				"When emergency mode is on, and emergency mode was on, and the turn indicator lever switches to 2, the system shall assign 3 to the mode and reset the timer.");
		reqList.add(
				"When emergency mode is on, and emergency mode was on, and the turn indicator lever switches to 0, and the mode is not 0, the system shall assign 0 to the mode and reset the timer.");
		reqList.add(
				"When the emergency mode switches to off, and the turn indicator lever is 1, and the turn indicator lever was 1, and the mode is not 1, the system shall assign 1 to the mode and reset the timer.");
		reqList.add(
				"When the emergency mode switches to off, and the turn indicator lever is 2, and the turn indicator lever was 2, and the mode is not 3, the system shall assign 3 to the mode and reset the timer.");

		inputs.add(new Input("the voltage", DataType.number, "0", "100", "0"));
		inputs.add(new Input("the turn indicator lever", DataType.number, "0", "2", "0"));
		inputs.add(new Input("emergency mode", DataType.bool, "false"));

		outputs.add(new Output("the indication lights", DataType.number, "0", "3", "2"));
		outputs.add(new Output("the mode", DataType.number, "0", "3", "2"));
		outputs.add(new Output("the timer", DataType.timer, "-1", "100", "0"));

		estimationToReachOutputState = 150;
		for (int i = 0; i < reqList.size(); i++) {
			idToRequirements.put(String.valueOf(i), reqList.get(i));
		}
		systemName = "TIS";
	}

	private static void testLightsControllerComponent() {
		String text = "When the voltage is greater than 80, and the timer was greater than or equal to 220, and the indication lights 1 are off, and the indication lights 2 are off, and the mode is 1 or 2, the lights controller component shall set on to indication lights 1 , and the system shall assign off to the indication lights 2 and reset the timer.";

		inputs.add(new Input("voltage", DataType.number, "1", "100", "0"));
		inputs.add(new Input("mode", DataType.number, "1", "3", "1"));

		outputs.add(new Output("timer", DataType.number, "0", "225", "0"));
		outputs.add(new Output("indication lights 1", DataType.bool, "false"));
		outputs.add(new Output("indication lights 2", DataType.bool, "false"));

		idToRequirements.put("1", text);
		systemName = "LightsControllerComponent";
	}

	private static void testSwitch() {
		String text = "When the mode switches from on to off and the mode switches from false to true, the system shall do nothing.";
		String text2 = "When the mode switches from 1 to 3 and the mode switches from 1 to greater than 3 and the mode switches from greater than 1 to 3 and the mode switches from less than 1 to greater than 3, the system shall do nothing.";
		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "SwitchTest";
	}

	private static void testAddSubtract() {
		String text = "When x is 2,then the system shall add 10 to y.";
		String text2 = "When x is 3, then the system shall subtract 20 from y.";

		inputs.add(new Input("x", DataType.number, "0", "100", "1"));
		outputs.add(new Output("y", DataType.number, "0", "100", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "AddSubtractTest";
	}

	private static void testAddSubtract2() {
		String text = "When x is 2,then the system shall add 10 to y and the system shall assign 3 to x.";
		String text2 = "When x is 3, then the system shall subtract 20 from y.";

		inputs.add(new Input("x", DataType.number, "0", "100", "1"));
		outputs.add(new Output("y", DataType.number, "0", "100", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "AddSubtractTest2";
	}

	private static void testNoCondition() {
		String text = "x is lower than 100, and x is higher than 0.";

		inputs.add(new Input("x", DataType.number, "1", "99", "1"));

		idToRequirements.put("1", text);
		systemName = "NoCondition";
	}

	private static void testOutputInCondition() {
		String text = "When x is 3, or x is 2,then the system shall set y to 3.";
		String text1 = "When y is 3,then x is 2 or 3.";
		String text2 = "When y is 3,then the system shall set z to 10.";

		inputs.add(new Input("x", DataType.number, "1", "99", "1"));
		outputs.add(new Output("y", DataType.number, "1", "99", "1"));
		outputs.add(new Output("z", DataType.number, "1", "99", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text1);
		idToRequirements.put("3", text2);
		systemName = "OutputInCondition";
	}

	private static void testChanges() {
		String text = "When y changes its value, then the system shall set z to 3.";
		String text1 = "When x is 3,then the system shall set y to 10.";

		inputs.add(new Input("x", DataType.number, "1", "99", "1"));
		outputs.add(new Output("y", DataType.number, "1", "99", "1"));
		outputs.add(new Output("z", DataType.number, "1", "99", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text1);
		systemName = "ChangeTest";
	}

	private static void testOutputBehavior() {
		String text = "When current_position changes its value, then the system shall subtract 1 from current_position or it shall add 1 to current_position.";
		String text1 = "When x is 1 and current_position is 100, then the system shall set z to 10.";

		inputs.add(new Input("x", DataType.number, "1", "99", "1"));
		outputs.add(new Output("current_position", DataType.number, "0", "100", "0"));
		outputs.add(new Output("z", DataType.number, "1", "99", "1"));
		estimationToReachOutputState = 100;
		idToRequirements.put("1", text);
		idToRequirements.put("2", text1);
		systemName = "OutputBehaviorTest";
	}

	private static void testAndOrConnection() {
		String text = "When x is 2, and y1 is 3, or y1 is 4, then y1 is 10, or y1 is 20 and x is 20.";
		String text1 = "When x is 5, then y1 or z1 is 5 and y1 or z1 is 6.";
		String text2 = "When x is 7, then y1 and z1 is 5, or y1 and z1 is 6.";
		String text3 = "When x is 6, then y1 or z1 is 5 or 4 and y1 or z1 is 6 or 7.";
		String text4 = "When x is 8, then y1 and z1 is 5 or 4 or y1 and z1 is 6 or 7.";
		inputs.add(new Input("x", DataType.number, "1", "99", "1"));
		outputs.add(new Output("y1", DataType.number, "1", "99", "1"));
		outputs.add(new Output("z1", DataType.number, "1", "99", "1"));
		idToRequirements.put("1", text);
		idToRequirements.put("2", text1);
		idToRequirements.put("3", text2);
		idToRequirements.put("4", text3);
		idToRequirements.put("5", text4);
		systemName = "AndOrTest";
	}

	private static void testPresentation() {
		String text = "When x is greater than 2,then the system shall add 10 to y.";
		String text2 = "When x is 0 or 1, then the system shall set x to 10 after 3 seconds.";

		inputs.add(new Input("x", DataType.number, "0", "100", "1"));
		outputs.add(new Output("y", DataType.number, "0", "100", "1"));

		estimationToReachOutputState = 1;
		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "testPresentation";
	}

	private static void testPresentation4() {
		String text = "When x is greater than 2, then the system shall add 10 to y.";
		String text2 = "When x is 0 or 1, then the system shall set x to 10.";

		outputs.add(new Output("x", DataType.number, "0", "100", "1"));
		outputs.add(new Output("y", DataType.number, "0", "100", "1"));

		estimationToReachOutputState = 1;
		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "testPresentation";
	}

	private static void testPresentation2() {
		String text = "When x is 2,then y is 10 and x is 3.";
		String text2 = "When x is 3, then the system shall assign 20 to y.";

		inputs.add(new Input("x", DataType.number, "0", "100", "1"));
		outputs.add(new Output("y", DataType.number, "0", "100", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "testPresentation2";
	}

	private static void testPresentation3() {
		String text = "When x is 1 or 2, switch x to 10 after 3 seconds.";
		String text2 = "When x is 10, switch x to 20 after 2 seconds.";

		outputs.add(new Output("x", DataType.number, "0", "225", "1"));

		idToRequirements.put("1", text);
		idToRequirements.put("2", text2);
		systemName = "testPresentation3";
	}

	private static void testOrSplitting() {
		String text = "When x is greater or equal to 10, and y is 1 or 2, and z is 3 or 4, the system shall switch x to 10 after 3 seconds.";

		outputs.add(new Output("x", DataType.number, "0", "225", "1"));
		outputs.add(new Output("y", DataType.number, "0", "225", "1"));
		outputs.add(new Output("z", DataType.number, "0", "225", "1"));

		idToRequirements.put("1", text);
		systemName = "testPresentation3";
	}

}
