package testgeneration;

import java.util.List;
import java.util.Map;

import export.CsvExport;
import model.Input;
import model.Output;
import slr.Preprocessor;
import slr.RoleLabeler;
import slr.SRLData;
import z3.Generator;

public class TestGenerator {

	private Preprocessor preprocessor = new Preprocessor();
	private RoleLabeler srl = new RoleLabeler();
	private TRExtractor extractor = new TRExtractor();
	private FunctionGenerator functionGen = new FunctionGenerator();
	private Generator testCaseGen = new Generator();
	private CsvExport exporter = new CsvExport();

	public void generateTests(String systemName, Map<String, String> idToRequirements, List<Input> inputs,
			List<Output> outputs, int estimationToReachOutputState) {

		// preprocess requirements
		idToRequirements = preprocessor.modifyRequirements(idToRequirements, inputs, outputs);

		// semantic role labeling
		SRLData data = srl.analyze(idToRequirements, inputs, outputs);

		// extract thematic roles via rules
		extractor.extractRoles(idToRequirements, data, inputs, outputs);

		// generate functions that represent the requirement set
		functionGen.generateFunctions(systemName, idToRequirements, data, inputs, outputs);

		// generate test cases with z3
		testCaseGen.generateTestSuite(data, inputs, outputs, estimationToReachOutputState);

		// export as time value pairs for evaluation
		exporter.export(data.getTestSuites());

	}

}
