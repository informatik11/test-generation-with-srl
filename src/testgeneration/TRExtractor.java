package testgeneration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import model.Enums.ComparisonOperator;
import model.Enums.DataType;
import model.Enums.MathOperator;
import model.Input;
import model.Output;
import model.PredicateFrame;
import model.RequirementFrame;
import model.SRLRelation;
import model.Signal;
import slr.SRLData;

// maps sematic roles from slr to thematic roles
public class TRExtractor {

	public void extractRoles(Map<String, String> idToRequirements, SRLData data, List<Input> inputs,
			List<Output> outputs) {
		// determine whether the predicates are contained in a condition or not
		for (RequirementFrame reqFrame : data.getRequirementFrames()) {
			for (PredicateFrame predFrame : reqFrame.getPredicateFrames()) {
				// TODO a simplifying assumption is made: the conditional part of a
				// sentence comes first
				// this works for the data that is currently evaluated, but needs to be
				// generalized
				// concept: in SRLData.extractConditionActionSentenceParts() identify an
				// interval for
				// each condition / action instead of only a separator
				// here we can then check to which interval a given predicate belongs
				if (predFrame.getPredicate().getStartSpan() < reqFrame.getCondActionSeparatorIndex()) {
					predFrame.setIsCondition(true);
				} else {
					predFrame.setIsCondition(false);
				}
			}
		}
		// for each input and output, extract the thematic roles from the slr roles
		List<Signal> signals = new ArrayList<Signal>();
		signals.addAll(inputs);
		signals.addAll(outputs);
		for (Signal signal : signals) {
			for (RequirementFrame reqFrame : data.getRequirementFrames()) {
				for (PredicateFrame predFrame : reqFrame.getPredicateFrames()) {
					IntPair span = extractSignalSpan(signal, predFrame);
					// if != null, the signal is contained in one of the relations of the predFrame
					if (span != null) {
						// TODO the following processing happens independent of the invoked frameset of
						// a predicate, reason: the searched information is currently not reliably
						// contained in a specific role of a predicate (given an invoked frameset)
						// with increasing SRL quality, this should be adapted to consider the invoked
						// frameset and the semantics of the invoked roles to increase robustness of the
						// algorithm
						predFrame.addSignal(signal, span.getFirst(), span.getSecond());
						predFrame.setDataType(signal.getDatatype());
						identifySignalChanges(signal, predFrame);
						identifyTimeAttributes(signal, predFrame);
					}
				}
			}
		}

		// DEBUG
		for (RequirementFrame reqFrame : data.getRequirementFrames()) {
			System.out.println(reqFrame.toString());
		}

	}

	private void identifySignalChanges(Signal signal, PredicateFrame predFrame) {

		if (predFrame.getPredicateText().equals("switch")) {
			extractRolesFromSwitch(signal, predFrame);
		} else {
			identifyNegation(predFrame);
			identifyMathOperator(predFrame);
			identifyComparisonOperator(predFrame);
			identifyToValue(signal, predFrame);

		}
	}

	private void identifyNegation(PredicateFrame predFrame) {
		String mergedText = concatRelations(predFrame, new String[] { "AM-LOC", "AM-MNR" });
		if (containsWithRegEx(mergedText, "\\b" + "not" + "\\b")) {
			predFrame.setNegated(true);
		}
	}

	private void identifyMathOperator(PredicateFrame predFrame) {
		// todo: only extract operator if the correct role from the predicate is
		// invoked, e.g. add needs to invoke the role add.02 from PropBank to represent
		// mathematical addition
		if (predFrame.getPredicateText().equals("add")) {
			predFrame.setMathOperator(MathOperator.add);
		} else if (predFrame.getPredicateText().equals("subtract")) {
			predFrame.setMathOperator(MathOperator.subtract);
		} else if (predFrame.getPredicateText().equals("multiply")) {
			predFrame.setMathOperator(MathOperator.multiply);
		} else if (predFrame.getPredicateText().equals("divide")) {
			predFrame.setMathOperator(MathOperator.divide);
		}

	}

	private void identifyTimeAttributes(Signal signal, PredicateFrame predFrame) {
		String mergedText = concatRelations(predFrame, new String[] { "AM-TMP", "AM-ADV" });

		if (containsWithRegEx(mergedText, "\\b" + "after" + "\\b")) {
			List<String> result = new ArrayList<String>();
			List<Double> values = extractNumber(predFrame, mergedText);
			for (Double number : values) {
				result.add(number.toString());
			}
			predFrame.addAfterValues(result);
			signal.addPossibleValues(result);
		}
		if (containsWithRegEx(mergedText, "\\b" + "for" + "\\b")) {
			List<String> result = new ArrayList<String>();
			List<Double> values = extractNumber(predFrame, mergedText);
			for (Double number : values) {
				result.add(number.toString());
			}
			predFrame.addForValues(result);
			signal.addPossibleValues(result);
		}
	}

	private void extractRolesFromSwitch(Signal signal, PredicateFrame predFrame) {
		String mergedText;

		mergedText = concatRelations(predFrame, new String[] { "A3", "A2", "AM-MNR", "AM-TMP" });
		if (signal.getDatatype().equals(DataType.bool)) {
			// values are boolean, so there are no modifiers
			// extract from value
			String pattern = "from(\\s+)(on|off|false|true)";
			String fromValue = findFirstMatch(mergedText, pattern);
			fromValue = fromValue.replaceFirst("from(\\s+)", "");
			Boolean fromValueBool = mapBooleanStringToBoolean(fromValue);
			if (fromValueBool != null) {
				predFrame.addFromVal(fromValueBool.toString());
				signal.addPossibleValue(fromValueBool.toString());
			}

			// extract to value
			pattern = "to(\\s+)(on|off|false|true)";
			String toValue = findFirstMatch(mergedText, pattern);
			toValue = toValue.replaceFirst("to(\\s+)", "");
			Boolean toValueBool = mapBooleanStringToBoolean(toValue);
			if (toValueBool != null) {
				predFrame.addToVal(toValueBool.toString());
				signal.addPossibleValue(toValueBool.toString());
			}

		} else {
			// values are numbers

			// extract from modifier if it exists
			String pattern = "from(\\D)*(-?\\d+)";
			String modifier = findFirstMatch(mergedText, pattern);
			modifier = modifier.replaceFirst("from(\\s+)", "");
			modifier = modifier.replaceFirst("(-?\\d+)", "");
			if (!modifier.isEmpty()) {
				ComparisonOperator mod = mapModifierToComparisonOperator(modifier);
				predFrame.setFromModifier(mod);
				// remove modifier
				mergedText = mergedText.replaceFirst(modifier, "");
			}

			// extract from value
			pattern = "from(\\s+)(-?\\d+)";
			String fromValue = findFirstMatch(mergedText, pattern);
			fromValue = fromValue.replaceFirst("from(\\s+)", "");
			predFrame.addFromVal(fromValue);
			if (fromValue != "") {
				predFrame.addToVal(fromValue.toString());
				signal.addPossibleValue(fromValue.toString());
			}

			// extract to modifier if it exists
			pattern = "to(\\D)*(-?\\d+)";
			modifier = findFirstMatch(mergedText, pattern);
			modifier = modifier.replaceFirst("to(\\s+)", "");
			modifier = modifier.replaceFirst("(-?\\d+)", "");
			if (!modifier.isEmpty()) {
				ComparisonOperator mod = mapModifierToComparisonOperator(modifier);
				predFrame.setModifier(mod);
				// remove modifier
				mergedText = mergedText.replaceFirst(modifier, "");
			} else {
				predFrame.setModifier(ComparisonOperator.equal);
			}

			// extract to value
			pattern = "to(\\s+)(-?\\d+)";
			String toValue = findFirstMatch(mergedText, pattern);
			toValue = toValue.replaceFirst("to(\\s+)", "");
			predFrame.addToVal(toValue);
			if (toValue != "") {
				predFrame.addToVal(toValue.toString());
				signal.addPossibleValue(toValue.toString());
			}

		}
	}

	private ComparisonOperator mapModifierToComparisonOperator(String modifier) {
		if (modifier.contains(">=")
				|| ((modifier.contains("greater") || modifier.contains("more") || modifier.contains("higher"))
						&& modifier.contains("equal")))
			return ComparisonOperator.greaterOrEqual;
		if (modifier.contains("<=")
				|| ((modifier.contains("smaller") || modifier.contains("less") || modifier.contains("lower"))
						&& modifier.contains("equal")))
			return ComparisonOperator.lessOrEqual;
		if (modifier.contains("<")
				|| (modifier.contains("smaller") || modifier.contains("less") || modifier.contains("lower")))
			return ComparisonOperator.less;
		if (modifier.contains(">")
				|| (modifier.contains("greater") || modifier.contains("more") || modifier.contains("higher")))
			return ComparisonOperator.greater;
		if (modifier.contains("=") || (modifier.contains("equal")))
			return ComparisonOperator.equal;
		return ComparisonOperator.equal;
	}

	private Boolean mapBooleanStringToBoolean(String fromValue) {
		if (fromValue.contains("off") || fromValue.contains("false")) {
			return false;
		}
		if (fromValue.contains("on") || fromValue.contains("true")) {
			return true;
		}
		return null;
	}

	private String findFirstMatch(String mergedText, String pattern) {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(mergedText);
		if (m.find()) {
			return m.group();
		}
		return "";
	}

	private String concatRelations(PredicateFrame predFrame, String[] relations) {
		StringBuilder concatenatedRelation = new StringBuilder();
		for (String relation : relations) {
			List<SRLRelation> text = predFrame.getRelation(relation);
			for (SRLRelation srlRelation : text) {
				concatenatedRelation.append(srlRelation.getContent() + " ");
			}
		}
		return concatenatedRelation.toString();
	}

	private IntPair extractSignalSpan(Signal signal, PredicateFrame predFrame) {
		if (relationContainsString(predFrame, "AM-MNR", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("AM-MNR"), signal.getName());
		}
		if (relationContainsString(predFrame, "AM-TMP", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("AM-TMP"), signal.getName());
		}
		if (relationContainsString(predFrame, "AM-ADV", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("AM-ADV"), signal.getName());
		}
		if (relationContainsString(predFrame, "A1", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("A1"), signal.getName());
		}
		if (relationContainsString(predFrame, "A0", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("A0"), signal.getName());
		}
		if (relationContainsString(predFrame, "A2", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("A2"), signal.getName());
		}
		if (relationContainsString(predFrame, "AM-MOD", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("AM-MOD"), signal.getName());
		}
		if (relationContainsString(predFrame, "AM-LOC", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("AM-LOC"), signal.getName());
		}
		if (relationContainsString(predFrame, "A4", signal.getName())) {
			return calculateSignalSpanFromRelation(predFrame.getRelation("A4"), signal.getName());
		}
		return null;
	}

	private IntPair calculateSignalSpanFromRelation(List<SRLRelation> relationsWithSameName, String signalName) {

		for (SRLRelation relation : relationsWithSameName) {
			String relationContent = " " + relation.getContent() + " ";
			String pattern = "\\b" + signalName + "\\b";
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(relationContent);
			// calculate signal start span offset
			int relationStartSpan = relation.getStartSpan();
			if (m.find()) {
				int signalStartSpan = m.start() + relationStartSpan;
				int signalEndSpan = m.end() + relationStartSpan;
				IntPair result = new IntPair(signalStartSpan, signalEndSpan);
				return result;
			}
		}

		return null;
	}

	private boolean relationContainsString(PredicateFrame predFrame, String relation, String name) {
		List<SRLRelation> text = predFrame.getRelation(relation);
		StringBuilder concatenatedRelation = new StringBuilder();
		for (SRLRelation srlRelation : text) {
			concatenatedRelation.append(srlRelation.getContent() + " ");
		}
		String pattern = "\\b" + name + "\\b";
		String result = findFirstMatch(concatenatedRelation.toString(), pattern);
		if (result != "")
			return true;
		else
			return false;
	}

	// extracts all values that are connected via an "or" from a string
	private List<String> extractValues(PredicateFrame predFrame, String text) {
		List<String> result = new ArrayList<String>();

		if (predFrame.getDatatype().equals(DataType.bool)) {
			List<Boolean> bools = extractBool(predFrame, text);
			for (Boolean bool : bools) {
				if (!result.contains(bool.toString()))
					result.add(bool.toString());
			}
		} else { // timer or number as datatype
			List<Double> numbers = extractNumber(predFrame, text);
			for (Double number : numbers) {
				result.add(number.toString());
			}
		}
		return result;
	}

	private List<Boolean> extractBool(PredicateFrame predFrame, String text) {
		List<Boolean> result = new ArrayList<Boolean>();

		// TODO current extraction is very simple and not robust
		if (containsWithRegEx(text.toLowerCase(), "\\b" + "on" + "\\b")
				|| containsWithRegEx(text.toLowerCase(), "\\b" + "true" + "\\b")) {
			result.add(true);
		}
		if (containsWithRegEx(text.toLowerCase(), "\\b" + "off" + "\\b")
				|| containsWithRegEx(text.toLowerCase(), "\\b" + "false" + "\\b")) {
			if (result.isEmpty())
				result.add(false);
			else {
				// if true has already been extracted, assume there might only be an "off" or
				// "false" if
				// the sentence contains an "or"
				if (containsWithRegEx(text.toLowerCase(), "\\b" + "or" + "\\b")) {
					result.add(false);
				}
			}
		}

		return result;
	}

	private List<Double> extractNumber(PredicateFrame predFrame, String text) {
		List<Double> result = new ArrayList<Double>();
		Pattern p = Pattern.compile("\\b" + "[0-9]+" + "\\b");
		if (text != "") {
			text = " " + text + " ";
			Matcher m = p.matcher(text);
			while (m.find()) {
				result.add(Double.valueOf(m.group()));
				// if the text does not contain an or, do not search for other numbers
				if (!containsWithRegEx(text.toLowerCase(), "\\b" + "or" + "\\b"))
					break;
			}

		}
		return result;
	}

	private void identifyToValue(Signal signal, PredicateFrame predFrame) {
		// check AM-MNR, afterwards check multiple tags simulatenously
		String text = concatRelations(predFrame, new String[] { "AM-MNR" });
		List<String> values = extractValues(predFrame, text);
		if (values.isEmpty()) {
			text = concatRelations(predFrame,
					new String[] { "A1", "AM-MNR", "AM-TMP", "AM-ADV", "A2", "A3", "AM-LOC" });
			values = extractValues(predFrame, text);
		}

		predFrame.addToValues(values);
		signal.addPossibleValues(values);

	}

	private void identifyComparisonOperator(PredicateFrame predFrame) {
		String text = concatRelations(predFrame, new String[] { "AM-MNR", "AM-ADV", "AM-TMP", "A1", "A2" });
		ComparisonOperator result = mapModifierToComparisonOperator(text);
		// TODO handle multiple matches
		if (result != null)
			predFrame.setModifier(result);
	}

	private boolean containsWithRegEx(String text, String pattern) {
		String result = findFirstMatch(text, pattern);
		if (result != "")
			return true;
		else
			return false;
	}

}
