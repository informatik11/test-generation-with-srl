package cogcompnlp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.annotation.AnnotatorService;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TokenLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.View;
import edu.illinois.cs.cogcomp.core.resources.ResourceConfigurator;
import edu.illinois.cs.cogcomp.pipeline.main.PipelineFactory;

public class cogCompNLP {

	private static AnnotatorService pipeline = null;

	/**
	 * first element in the list is srl_verb, second is srl_comma
	 * 
	 * @param corpusID
	 * @param textID
	 * @param text
	 * @return
	 */
	public static List<View> generatePredicateArgumentView(String corpusID, String textID, String text) {

		ResourceConfigurator.ENDPOINT.value = "http://macniece.seas.upenn.edu:4008";
		PredicateArgumentView slrVerb = null;
		PredicateArgumentView slrComma = null;
		TokenLabelView pos = null;
		try {
			if (pipeline == null) {
				pipeline = PipelineFactory.buildPipeline(ViewNames.POS, ViewNames.LEMMA,
						ViewNames.PARSE_STANFORD, ViewNames.SHALLOW_PARSE, ViewNames.NER_CONLL, ViewNames.SRL_VERB,
						ViewNames.SRL_COMMA);
			}
			TextAnnotation ta = pipeline.createAnnotatedTextAnnotation(corpusID, textID, text);
			slrVerb = (PredicateArgumentView) ta.getView(ViewNames.SRL_VERB);
			slrComma = (PredicateArgumentView) ta.getView(ViewNames.SRL_COMMA);
			pos = (TokenLabelView) ta.getView(ViewNames.POS);
			// DEBUG
			// System.out.println(slrVerb);
			// System.out.println(slrComma);

		} catch (AnnotatorException | IOException e) {
			e.printStackTrace();
		}

		List<View> result = new ArrayList<View>();
		result.add(slrVerb);
		result.add(slrComma);
		result.add(pos);
		return result;
	}

}
